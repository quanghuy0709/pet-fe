/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,ts}', './node_modules/flowbite/**/*.js'],
  theme: {
    extend: {
      width: {
        18: '4.5rem',
      },
      height: {
        18: '4.5rem',
      },
      colors: {
        'sprinted-orange-salmon': '#FFE5D6',
        'sprinted-orange': '#D95C26',
        'sprinted-orange-text': '#ED7838',
        'comment-orange': '#EB5200',
        'comment-orange-light': '#FF8B42',
        'button-orange': '#F08F52',
        brown: '#80756E',
        'green-label': '#1CBD39',
        'cyan-label': '#3495BF',
        'blue-label': '#2C4FA3',
        'sprinted-blue': '#ced5ea',
        'red-label': '#E82525',
        'yellow-label': '#FFB908',
        'orange-label': '#FF7119',
        'green-badge': '#e6f4e7',
        'blue-badge': '#e4eef5',
        'red-badge': '#f3dede',
      },
      screens: {
        ssm: '120px',
        tablet: '640px',
        desktop: '1280px',
      },
      borderRadius: {
        '4xl': '50px',
      },
    },
    corePlugins: {
      preflight: false,
      important: '#__next',
    },
  },
  plugins: [require('flowbite/plugin')],
};
