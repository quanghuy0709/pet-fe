export const environment = {
  production: true,
  BASE_URL: 'https://api.sprinted.it',
  MAX_SIZE_IMAGE_IN_MB: 5000 * 1024,
};
