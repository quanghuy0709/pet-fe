//Fix dinamici
// document.querySelector('main').style.marginTop = document.getElementById('header').offsetHeight + 'px';

lazyLoadInstance = new LazyLoad();

// const width = window.innerWidth;
// const height = window.innerHeight;

$(document).ready(function () {
  if (navigator.userAgent.indexOf('Mobile') != -1 && (navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('Android') != -1)) {
    openDownloadApp();
  }
  setTimeout(function () {
    $(".dashboardSidebar").height(window.innerHeight - 72);
  }, 200);
});

$('.appDownloader').on('click', function () {
  openDownloadApp();
});

$('.comingSoonOpener').on('click', function () {
  openComingSoon();
});

function openDownloadApp() {
  $('.downloadApp').addClass('appear');
  setTimeout(function () {
    $('.downloadApp .content').addClass('appear');
  }, 100);
  if (navigator.userAgent.indexOf('iPhone') != -1) {
    $('.downloadApp .googlePlay').hide();
  } else if (navigator.userAgent.indexOf('Android') != -1) {
    $('.downloadApp .appStore').hide();
  }
}

function openComingSoon() {
  $('.comingSoon').addClass('appear');
  setTimeout(function () {
    $('.comingSoon .content').addClass('appear');
  }, 100);
}

$('.downloadApp .close').on('click', function () {
  $('.downloadApp').removeClass('appear');
});
$('.comingSoon .close').on('click', function () {
  $('.comingSoon').removeClass('appear');
});

$('.searchTop > div').on('click', function () {
  $(this).addClass('selected');
  $(this).siblings().removeClass('selected');
  $(this)
    .closest('.searchTop')
    .siblings('.searchBars')
    .find('.' + $(this).attr('for'))
    .show();
  $(this)
    .closest('.searchTop')
    .siblings('.searchBars')
    .find('.' + $(this).attr('for'))
    .siblings()
    .hide();
});

$('.mobileButtons').on('click', function () {
  $(this).toggleClass('opened');
  $('.headerMiddle').toggleClass('show');
  // $(this).siblings().removeClass("selected");
  // $(this).closest('.searchTop').siblings('.searchBars').find("." + $(this).attr("for")).show();
  // $(this).closest('.searchTop').siblings('.searchBars').find("." + $(this).attr("for")).siblings().hide();
});

function isInViewport(el) {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

document.addEventListener(
  'scroll',
  (event) => {
    $(".dashboardSidebar").height(window.innerHeight - 72);
    const box = document.querySelector('.searchContainer');
    if (window.innerWidth > 1023) {
      if (isInViewport(box)) {
        $('.headerMiddle').removeClass('show');
        $('.headerLeft').removeClass('scrolled');
      } else {
        $('.headerMiddle').addClass('show');
        $('.headerLeft').addClass('scrolled');
      }
    } else {
      if (isInViewport(box)) {
        $('.mobileButtons').removeClass('show');
      } else {
        $('.mobileButtons').addClass('show');
      }
    }
  },
  {
    passive: true,
  }
);
