import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileProfessionalContactComponent } from './profile-professional-contact.component';

@NgModule({
  declarations: [ProfileProfessionalContactComponent],
  imports: [CommonModule],
  exports: [ProfileProfessionalContactComponent],
})
export class ProfileProfessionalContactModule {}
