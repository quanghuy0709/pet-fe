import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileProfessionalContactComponent } from './profile-professional-contact.component';

describe('ProfileProfessionalContactComponent', () => {
  let component: ProfileProfessionalContactComponent;
  let fixture: ComponentFixture<ProfileProfessionalContactComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileProfessionalContactComponent]
    });
    fixture = TestBed.createComponent(ProfileProfessionalContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
