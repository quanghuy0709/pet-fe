import { Component } from '@angular/core';

@Component({
  selector: 'app-profile-professional-calendar',
  templateUrl: './profile-professional-calendar.component.html',
  styleUrls: ['./profile-professional-calendar.component.scss'],
})
export class ProfileProfessionalCalendarComponent {}
