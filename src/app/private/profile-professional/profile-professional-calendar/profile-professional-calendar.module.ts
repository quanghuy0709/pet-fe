import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { ProfileProfessionalCalendarComponent } from './profile-professional-calendar.component';

@NgModule({
  declarations: [ProfileProfessionalCalendarComponent],
  imports: [CommonModule, MatCardModule, MatDatepickerModule, MatNativeDateModule],
  exports: [ProfileProfessionalCalendarComponent],
})
export class ProfileProfessionalCalendarModule {}
