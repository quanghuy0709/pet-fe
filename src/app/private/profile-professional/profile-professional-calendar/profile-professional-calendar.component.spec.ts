import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileProfessionalCalendarComponent } from './profile-professional-calendar.component';

describe('ProfileProfessionalCalendarComponent', () => {
  let component: ProfileProfessionalCalendarComponent;
  let fixture: ComponentFixture<ProfileProfessionalCalendarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileProfessionalCalendarComponent]
    });
    fixture = TestBed.createComponent(ProfileProfessionalCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
