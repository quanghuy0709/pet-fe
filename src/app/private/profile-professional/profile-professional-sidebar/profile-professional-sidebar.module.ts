import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileProfessionalSidebarComponent } from './profile-professional-sidebar.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ProfileProfessionalSidebarComponent],
  imports: [CommonModule, RouterModule],
  exports: [ProfileProfessionalSidebarComponent],
})
export class ProfileProfessionalSidebarModule {}
