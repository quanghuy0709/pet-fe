import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileProfessionalSidebarComponent } from './profile-professional-sidebar.component';

describe('ProfileProfessionalSidebarComponent', () => {
  let component: ProfileProfessionalSidebarComponent;
  let fixture: ComponentFixture<ProfileProfessionalSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileProfessionalSidebarComponent]
    });
    fixture = TestBed.createComponent(ProfileProfessionalSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
