import { Component } from '@angular/core';

@Component({
  selector: 'app-profile-professional',
  templateUrl: './profile-professional.component.html',
  styleUrls: ['./profile-professional.component.scss'],
})
export class ProfileProfessionalComponent {}
