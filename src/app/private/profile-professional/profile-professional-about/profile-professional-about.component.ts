import { Component } from '@angular/core';
import { LocalstorageService } from '../../../service/localstorage.service';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { environment } from 'src/environments/environment';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-profile-professional-about',
  templateUrl: './profile-professional-about.component.html',
  styleUrls: ['./profile-professional-about.component.scss'],
})
export class ProfileProfessionalAboutComponent {
  user: any;
  base64Output?: string;
  constructor(
    private readonly localstorageService: LocalstorageService,
    private readonly modalService: ModalService,
    private ng2ImgMax: Ng2ImgMaxService
  ) {
    this.user = localstorageService.get('user');
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];

    const allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    const fileName = file.name.toLowerCase();
    const fileExtension = fileName.split('.').pop();

    if (!allowedExtensions.includes(fileExtension)) {
      alert('El archivo seleccionado no es una imagen válida. Por favor, selecciona una imagen (jpg, jpeg, png o gif).');
      event.target.value = null;
      return;
    }

    if (file.size > environment.MAX_SIZE_IMAGE_IN_MB) {
      this.showModalMessage(`File troppo grande, dimensione massima consentita ${environment.MAX_SIZE_IMAGE_IN_MB> 1000 ? environment.MAX_SIZE_IMAGE_IN_MB/1024/1000 +'MB' : environment.MAX_SIZE_IMAGE_IN_MB/1024 +'KB' } `, 'Scegli un file più piccolo');
      event.target.value = null;
      return;
    }

    const percentageReduction = 0.95;
    const targetFileSize = file.size * (1 - percentageReduction);
    const maxSizeInMB = targetFileSize * 0.000001;

    this.compressImage(file, maxSizeInMB);
  }

  showModalMessage(title: string, message: string) {
    this.modalService.showMessage = {
      type: 'warning',
      title: title,
      message: message,
      visible: true,
    };
  }

  compressImage(file: File, maxSizeInMB: number) {
    this.ng2ImgMax.compressImage(file, maxSizeInMB).subscribe(
      (compressedImage: Blob) => {
        const reader = new FileReader();
        reader.readAsDataURL(compressedImage);
        reader.onload = () => {
          const base64Image = reader.result as string;
          this.base64Output = base64Image;
        };
      },
      (error: any) => {
        console.error(error);
      }
    );
  }
}
