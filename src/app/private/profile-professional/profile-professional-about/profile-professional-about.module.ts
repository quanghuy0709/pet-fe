import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileProfessionalAboutComponent } from './profile-professional-about.component';

@NgModule({
  declarations: [ProfileProfessionalAboutComponent],
  imports: [CommonModule],
  exports: [ProfileProfessionalAboutComponent],
})
export class ProfileProfessionalAboutModule {}
