import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileProfessionalAboutComponent } from './profile-professional-about.component';

describe('ProfileProfessionalAboutComponent', () => {
  let component: ProfileProfessionalAboutComponent;
  let fixture: ComponentFixture<ProfileProfessionalAboutComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileProfessionalAboutComponent]
    });
    fixture = TestBed.createComponent(ProfileProfessionalAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
