import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ProfileProfessionalRoutingModule } from './profile-professional-routing.module';
import { ProfileProfessionalComponent } from './profile-professional.component';
import { ProfileProfessionalCalendarModule } from './profile-professional-calendar/profile-professional-calendar.module';
import { ProfileProfessionalSidebarModule } from './profile-professional-sidebar/profile-professional-sidebar.module';
import { ProfileProfessionalAboutModule } from './profile-professional-about/profile-professional-about.module';
import { ProfileProfessionalContactModule } from './profile-professional-contact/profile-professional-contact.module';

@NgModule({
  declarations: [ProfileProfessionalComponent],
  imports: [
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CommonModule,
    ProfileProfessionalRoutingModule,
    ProfileProfessionalCalendarModule,
    ProfileProfessionalSidebarModule,
    ProfileProfessionalAboutModule,
    ProfileProfessionalContactModule,
  ],
})
export class ProfileProfessionalModule {}
