import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CODE, CodeGeneral } from 'src/app/models/codeGeneral.model';
import { Pet } from 'src/app/models/pet.model';

import { PetService } from 'src/app/service/pet/pet.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';

import { Ng2ImgMaxService } from 'ng2-img-max';
import { ModalService } from 'src/app/service/modal/modal.service';
import * as _ from 'lodash';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { CodesService } from 'src/app/service/codes/codes.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-pet-form',
  templateUrl: './pet-form.component.html',
  styleUrls: ['./pet-form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PetFormComponent implements OnInit {
  myForm?: FormGroup;
  isEditPet: boolean = false;
  optionsAnimal: CodeGeneral[] = [];
  optionsBreedAnimal: CodeGeneral[] = [];
  hasBreedType: boolean = false;
  currentItemCode: string = '';
  base64Output?: string;
  hasPets: boolean = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly petService: PetService,
    private readonly router: Router,
    private readonly dialogRef: MatDialogRef<PetFormComponent>,
    private readonly codeService: CodesService,
    private ng2ImgMax: Ng2ImgMaxService,
    private readonly modalService: ModalService,
    private readonly snackBarMat: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public dataDialog: any
  ) {}
  ngOnInit(): void {
    this.hasPets = this.dataDialog?.pets;
    this.myForm = this.formBuilder.group({
      id: [null],
      name: ['', [Validators.required]],
      coated: ['', []],
      microchip: ['', []],
      distinguishingMarks: ['', []],
      sex: [null],
      dateOfBirth: [''],
      kind: [null],
      breed: [''],
      extraInformations: [''],
      imgProfile: [''],
    });
    this.myForm?.get('breed')?.disable();

    this.codeService.getListCode(CODE.PetKind).subscribe({
      next: (codes) => (this.optionsAnimal = codes),
    });

    if (this.dataDialog?.currentPet) {
      this.myForm?.get('breed')?.enable();
      this.editPet(this.dataDialog.currentPet);
    }
  }

  editPet(pet: Pet) {
    const petForm = _.cloneDeep(pet);
    switch (petForm.sex) {
      case 'Femmina':
        petForm.sex = 'F';
        break;
      case 'Maschio':
        petForm.sex = 'M';
        break;
    }
    this.isEditPet = true;
    this.myForm?.patchValue(petForm);
  }

  selectTypeAnimal(value: any) {
    this.currentItemCode = this.optionsAnimal.find((option) => option.id == value)?.itemCode ?? '';
    this.codeService.searchBycodeCount(this.currentItemCode).subscribe({
      next: (items) => {
        this.myForm?.get('breed')?.reset();
        if (items.length > 0) {
          this.hasBreedType = true;
          this.myForm?.get('breed')?.enable();
          this.optionsBreedAnimal = items;
        } else {
          this.hasBreedType = false;
          this.myForm?.get('breed')?.disable();
        }
      },
    });
  }

  searchByCode(event: any) {
    const searchString = event.target.value;
    this.codeService.searchBycode(this.currentItemCode, searchString).subscribe({
      next: (items) => {
        this.optionsBreedAnimal = items;
      },
    });
  }

  closeModal(param?: any) {
    if (this.hasPets) {
      this.dialogRef.close(param);
    } else {
      this.snackBarMat.open('Devi prima aggiungere un pet', 'chiudi');
    }
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];

    const allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    const fileName = file.name.toLowerCase();
    const fileExtension = fileName.split('.').pop();

    if (!allowedExtensions.includes(fileExtension)) {
      this.showModalMessage(
        'Estensione non consentita',
        "Il file selezionato non è un'immagine valida. Seleziona un'immagine (jpg, jpeg, png o gif)."
      );
      event.target.value = null;
      return;
    }
    if (file.size > environment.MAX_SIZE_IMAGE_IN_MB) {
      this.showModalMessage(`File troppo grande, dimensione massima consentita ${environment.MAX_SIZE_IMAGE_IN_MB> 1000 ? environment.MAX_SIZE_IMAGE_IN_MB/1024/1000 +'MB' : environment.MAX_SIZE_IMAGE_IN_MB/1024 +'KB' } `, 'Scegli un file più piccolo');
      event.target.value = null;
      return;
    }

    const percentageReduction = 0.95;
    const targetFileSize = file.size * (1 - percentageReduction);
    const maxSizeInMB = targetFileSize * 0.000001;

    this.compressImage(file, maxSizeInMB);
  }

  showModalMessage(title: string, message: string) {
    this.modalService.showMessage = {
      type: 'warning',
      title: title,
      message: message,
      visible: true,
    };
  }

  closeModalMessage() {
    this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
  }

  compressImage(file: File, maxSizeInMB: number) {
    this.ng2ImgMax.compressImage(file, 10).subscribe(
      (compressedImage: Blob) => {
        const reader = new FileReader();
        reader.readAsDataURL(compressedImage);
        reader.onload = () => {
          const base64Image = reader.result as string;
          this.base64Output = base64Image;
          this.myForm?.controls['imgProfile'].patchValue(this.base64Output);
        };
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  addPet(form: FormGroup) {
    if (form.valid) {
      form.value.imgProfile = this.base64Output;
      const { id: idPet } = form.value;
      const petBody = new Pet(form.value);
      if (!this.isEditPet) {
        this.petService.createPet(petBody).subscribe({
          next: () => {
            this.router.navigate(['private']);
            this.hasPets = true;
            this.closeModal('success');
          },
        });
      } else {
        this.petService.updatePet(idPet, petBody).subscribe({
          next: () => {
            this.router.navigate(['private']);
            this.closeModal('success');
          },
        });
      }
    }
  }
}
