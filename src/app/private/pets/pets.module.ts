import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleMapsModule } from '@angular/google-maps';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { PetFormComponent } from './petForm/pet-form.component';
import { PetsRoutingModule } from './pets-routing.module';
import { PetsComponent } from './pets.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PetDetailComponent } from './pet-detail/pet-detail.component';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [PetsComponent, PetFormComponent, PetDetailComponent],
  imports: [
    CommonModule,
    PetsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    GoogleMapsModule,
  ],
})
export class PetsModule {}
