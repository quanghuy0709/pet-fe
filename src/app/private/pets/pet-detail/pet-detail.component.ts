import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { PetService } from 'src/app/service/pet/pet.service';

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styleUrls: ['./pet-detail.component.scss'],
})
export class PetDetailComponent {
  constructor(
    private readonly petService: PetService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    activatedRoute.params.pipe(switchMap((param) => this.petService.getPets()));
  }
}
