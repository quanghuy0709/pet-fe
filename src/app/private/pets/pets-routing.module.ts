import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PetsComponent } from './pets.component';
import { PetFormComponent } from './petForm/pet-form.component';

const routes: Routes = [
  {
    path: '',
    component: PetsComponent,
  },
  {
    path: 'form',
    component: PetFormComponent,
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PetsRoutingModule {}
