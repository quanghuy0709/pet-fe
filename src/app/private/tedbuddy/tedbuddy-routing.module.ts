import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TedbuddyComponent } from './tedbuddy.component';

const routes: Routes = [
  {
    path: '',
    component: TedbuddyComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
      {
        path: 'healthcare',
        loadChildren: () => import('../healthcare/healthcare.module').then((m) => m.HealthcareModule),
      },
      // {
      //   path: 'note',
      //   loadChildren: () => import('../notes/notes.module').then((m) => m.NotesModule),
      // },
      {
        path: 'messaggi',
        loadChildren: () => import('../messaggi/messaggi.module').then((m) => m.MessaggiModule),
      },
      {
        path: 'prenotazioni',
        loadChildren: () => import('../prenotazioni/prenotazioni.module').then((m) => m.PrenotazioniModule),
      },
      {
        path: 'nuts',
        loadChildren: () => import('../nuts/nuts.module').then((m) => m.NutsModule),
      },
      {
        path: 'pets',
        loadChildren: () => import('../pets/pets.module').then((m) => m.PetsModule),
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then((m) => m.ProfileModule),
      },
      {
        path: 'users',
        loadChildren: () => import('../users/users.module').then((m) => m.UsersModule),
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TedbuddyRoutingModule {}
