import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TedbuddyRoutingModule } from './tedbuddy-routing.module';
import { TedbuddyComponent } from './tedbuddy.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [TedbuddyComponent],
  imports: [CommonModule, TedbuddyRoutingModule, SharedModule],
})
export class TedbuddyModule {}
