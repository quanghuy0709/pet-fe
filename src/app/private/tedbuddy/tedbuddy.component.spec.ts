import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TedbuddyComponent } from './tedbuddy.component';

describe('TedbuddyComponent', () => {
  let component: TedbuddyComponent;
  let fixture: ComponentFixture<TedbuddyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TedbuddyComponent]
    });
    fixture = TestBed.createComponent(TedbuddyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
