import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessaggiRoutingModule } from './messaggi-routing.module';
import { MessaggiComponent } from './messaggi.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [MessaggiComponent],
  imports: [CommonModule, MessaggiRoutingModule, SharedModule],
})
export class MessaggiModule {}
