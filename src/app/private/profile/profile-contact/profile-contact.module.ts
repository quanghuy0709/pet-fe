import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileContactComponent } from './profile-contact.component';

@NgModule({
  declarations: [ProfileContactComponent],
  imports: [CommonModule],
  exports: [ProfileContactComponent],
})
export class ProfileContactModule {}
