import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePathologyComponent } from './profile-pathology.component';

describe('ProfilePathologyComponent', () => {
  let component: ProfilePathologyComponent;
  let fixture: ComponentFixture<ProfilePathologyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfilePathologyComponent]
    });
    fixture = TestBed.createComponent(ProfilePathologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
