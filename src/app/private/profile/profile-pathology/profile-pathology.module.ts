import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilePathologyRoutingModule } from './profile-pathology-routing.module';
import { ProfilePathologyComponent } from './profile-pathology.component';


@NgModule({
  declarations: [
    ProfilePathologyComponent
  ],
  imports: [
    CommonModule,
    ProfilePathologyRoutingModule
  ]
})
export class ProfilePathologyModule { }
