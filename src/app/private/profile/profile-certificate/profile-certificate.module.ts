import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileCertificateRoutingModule } from './profile-certificate-routing.module';
import { ProfileCertificateComponent } from './profile-certificate.component';


@NgModule({
  declarations: [
    ProfileCertificateComponent
  ],
  imports: [
    CommonModule,
    ProfileCertificateRoutingModule
  ]
})
export class ProfileCertificateModule { }
