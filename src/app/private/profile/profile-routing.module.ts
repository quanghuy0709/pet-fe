import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {
        path: 'about',
        loadChildren: () => import('../profile/profile-about/profile-about.module').then((m) => m.ProfileAboutModule),
      },
      {
        path: 'pathology',
        loadChildren: () => import('../profile/profile-pathology/profile-pathology.module').then((m) => m.ProfilePathologyModule),
      },
      {
        path: 'photo',
        loadChildren: () => import('../profile/profile-photo/profile-photo.module').then((m) => m.ProfilePhotoModule),
      },
      {
        path: 'video',
        loadChildren: () => import('../profile/profile-video/profile-video.module').then((m) => m.ProfileVideoModule),
      },
      {
        path: 'certificate',
        loadChildren: () => import('../profile/profile-certificate/profile-certificate.module').then((m) => m.ProfileCertificateModule),
      },
      {
        path: 'specialization',
        loadChildren: () => import('../profile/profile-specialization/profile-specialization.module').then((m) => m.ProfileSpecializationModule),
      },
      {
        path: 'publication',
        loadChildren: () => import('../profile/profile-publication/profile-publication.module').then((m) => m.ProfilePublicationModule),
      },
      {
        path: 'add-information',
        loadChildren: () => import('../profile/profile-add-information/profile-add-information.module').then((m) => m.ProfileAddInformationModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
