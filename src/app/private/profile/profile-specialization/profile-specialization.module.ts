import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileSpecializationRoutingModule } from './profile-specialization-routing.module';
import { ProfileSpecializationComponent } from './profile-specialization.component';


@NgModule({
  declarations: [
    ProfileSpecializationComponent
  ],
  imports: [
    CommonModule,
    ProfileSpecializationRoutingModule
  ]
})
export class ProfileSpecializationModule { }
