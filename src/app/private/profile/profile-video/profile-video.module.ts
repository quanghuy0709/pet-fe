import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileVideoRoutingModule } from './profile-video-routing.module';
import { ProfileVideoComponent } from './profile-video.component';


@NgModule({
  declarations: [
    ProfileVideoComponent
  ],
  imports: [
    CommonModule,
    ProfileVideoRoutingModule
  ]
})
export class ProfileVideoModule { }
