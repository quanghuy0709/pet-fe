import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileCalendarModule } from './profile-calendar/profile-calendar.module';
import { ProfileAboutModule } from './profile-about/profile-about.module';
import { ProfileSidebarModule } from './profile-sidebar/profile-sidebar.module';
import { ProfileContactModule } from './profile-contact/profile-contact.module';

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, ProfileRoutingModule, ProfileCalendarModule, ProfileAboutModule, ProfileSidebarModule, ProfileContactModule],
})
export class ProfileModule {}
