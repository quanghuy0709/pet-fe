import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilePublicationRoutingModule } from './profile-publication-routing.module';
import { ProfilePublicationComponent } from './profile-publication.component';


@NgModule({
  declarations: [
    ProfilePublicationComponent
  ],
  imports: [
    CommonModule,
    ProfilePublicationRoutingModule
  ]
})
export class ProfilePublicationModule { }
