import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileCalendarComponent } from './profile-calendar.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [ProfileCalendarComponent],
  imports: [CommonModule, MatCardModule, MatDatepickerModule, MatNativeDateModule],
  exports: [ProfileCalendarComponent],
})
export class ProfileCalendarModule {}
