import { Component } from '@angular/core';

@Component({
  selector: 'app-profile-calendar',
  templateUrl: './profile-calendar.component.html',
  styleUrls: ['./profile-calendar.component.scss'],
})
export class ProfileCalendarComponent {}
