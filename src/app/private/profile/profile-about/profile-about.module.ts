import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileAboutComponent } from './profile-about.component';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProfileAboutComponent],
  imports: [CommonModule, MatIconModule, MatGridListModule, ReactiveFormsModule, SharedModule],
  exports: [ProfileAboutComponent],
})
export class ProfileAboutModule {}
