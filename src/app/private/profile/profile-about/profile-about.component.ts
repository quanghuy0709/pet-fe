import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/service/localstorage.service';

import { Ng2ImgMaxService } from 'ng2-img-max';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth/auth.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/service/modal/modal.service';
import { MatDatepicker } from '@angular/material/datepicker';
import { map, switchMap } from 'rxjs';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { environment } from 'src/environments/environment';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-profile-about',
  templateUrl: './profile-about.component.html',
  styleUrls: ['./profile-about.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class ProfileAboutComponent implements OnInit {
  myForm!: FormGroup;
  user: any;
  base64Output?: string;
  constructor(
    private readonly localstorageService: LocalstorageService,
    private ng2ImgMax: Ng2ImgMaxService,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly modalService: ModalService,
    private readonly router: Router
  ) {
    this.user = localstorageService.get('user');
  }
  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      username: [this.user.data.username, [Validators.required]],
      name: [this.user.data.name, [Validators.required]],
      surname: [this.user.data.surname, [Validators.required]],
      dateOfBirth: [this.user.data.dateOfBirth],
      imgProfile: [this.user.data.imgProfile],
      address: [this.user.data.address.formattedAddress],
    });
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];

    if (!file) {
      return; // No se seleccionó ningún archivo
    }

    const allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    const fileName = file.name.toLowerCase();
    const fileExtension = fileName.split('.').pop();

    if (!allowedExtensions.includes(fileExtension)) {
      this.showModalMessage(
        'Estensione non consentita',
        "Il file selezionato non è un'immagine valida. Seleziona un'immagine (jpg, jpeg, png o gif)."
      );
      event.target.value = null;
      return;
    }
    if (file.size > environment.MAX_SIZE_IMAGE_IN_MB) {
      this.showModalMessage(`File troppo grande, dimensione massima consentita ${environment.MAX_SIZE_IMAGE_IN_MB> 1000 ? environment.MAX_SIZE_IMAGE_IN_MB/1024/1000 +'MB' : environment.MAX_SIZE_IMAGE_IN_MB/1024 +'KB' } `, 'Scegli un file più piccolo.');
      event.target.value = null;
      return;
    }

    const percentageReduction = 0.95;
    const targetFileSize = file.size * (1 - percentageReduction);
    const maxSizeInMB = targetFileSize * 0.000001;

    this.compressImage(file, maxSizeInMB);
  }

  compressImage(file: File, maxSizeInMB: number) {
    this.ng2ImgMax.compressImage(file, maxSizeInMB).subscribe(
      (compressedImage: Blob) => {
        this.processCompressedImage(compressedImage);
      },
      (error: any) => {
        console.error(error);
        this.showModalMessage(error.error, error.reason);
      }
    );
  }

  processCompressedImage(compressedImage: Blob) {
    const reader = new FileReader();
    reader.readAsDataURL(compressedImage);
    reader.onload = () => {
      const base64Image = reader.result as string;
      this.base64Output = base64Image;
      if (this.myForm?.controls['imgProfile']) {
        this.myForm.controls['imgProfile'].patchValue(this.base64Output);
      }
    };
  }

  showModalMessage(title: string, message: string) {
    this.modalService.showMessage = {
      type: 'warning',
      title: title,
      message: message,
      visible: true,
    };
  }

  updateProfile(form: FormGroup) {
    if (form.valid) {
      form.value.imgProfile = this.base64Output;
      const userBody = new User(form.value);
      this.authService
        .updateProfile(userBody)
        .pipe(
          switchMap(() => this.authService.profile()),
          map((user) => {
            this.localstorageService.set('user', user);
            this.router.navigate(['private/tedbuddy/profile']);
          })
        )
        .subscribe({
          next: () => {
            this.showModalMessage('Profilo aggiornato', 'Il tuo profilo è stato aggiornato con successo.');
            this.authService.profile();
          },
          error: (error) => {
            console.error('Error updating profile:', error);
          },
        });
    }
  }

  openDate(picker: MatDatepicker<any>, element: HTMLInputElement) {
    picker.open();
    element.focus();
  }

  showModalPasswordMessage() {
    this.showModalMessage('Cambia Password', 'Funzionalità non ancora disponibile');
  }

  closeModal() {
    this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
  }
}
