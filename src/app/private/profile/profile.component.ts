import { Component } from '@angular/core';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent {
  user: any;
  constructor(private readonly localstorageService: LocalstorageService) {
    this.user = localstorageService.get('user');
  }
}
