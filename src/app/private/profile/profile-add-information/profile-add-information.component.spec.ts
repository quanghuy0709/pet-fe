import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAddInformationComponent } from './profile-add-information.component';

describe('ProfileAddInformationComponent', () => {
  let component: ProfileAddInformationComponent;
  let fixture: ComponentFixture<ProfileAddInformationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileAddInformationComponent]
    });
    fixture = TestBed.createComponent(ProfileAddInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
