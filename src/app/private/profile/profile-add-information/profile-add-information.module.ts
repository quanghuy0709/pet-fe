import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileAddInformationRoutingModule } from './profile-add-information-routing.module';
import { ProfileAddInformationComponent } from './profile-add-information.component';


@NgModule({
  declarations: [
    ProfileAddInformationComponent
  ],
  imports: [
    CommonModule,
    ProfileAddInformationRoutingModule
  ]
})
export class ProfileAddInformationModule { }
