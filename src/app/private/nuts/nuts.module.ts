import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NutsRoutingModule } from './nuts-routing.module';
import { NutsComponent } from './nuts.component';
import { SharedModule } from "../../shared/shared.module";


@NgModule({
    declarations: [
        NutsComponent
    ],
    imports: [
        CommonModule,
        NutsRoutingModule,
        SharedModule
    ]
})
export class NutsModule { }
