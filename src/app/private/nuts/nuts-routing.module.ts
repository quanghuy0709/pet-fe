import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NutsComponent } from './nuts.component';

const routes: Routes = [
  {
    path: '',
    component: NutsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NutsRoutingModule {}
