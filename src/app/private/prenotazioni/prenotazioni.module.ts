import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrenotazioniRoutingModule } from './prenotazioni-routing.module';
import { PrenotazioniComponent } from './prenotazioni.component';
import { BryntumCalendarModule } from '@bryntum/calendar-angular';
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations: [PrenotazioniComponent],
    imports: [CommonModule, PrenotazioniRoutingModule, BryntumCalendarModule, SharedModule]
})
export class PrenotazioniModule {}
