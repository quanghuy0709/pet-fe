import { Component, ViewChild } from '@angular/core';
import { CalendarConfig, ProjectModelConfig } from '@bryntum/calendar';
import { BryntumCalendarComponent, BryntumProjectModelComponent } from '@bryntum/calendar-angular';

export const projectConfig: Partial<ProjectModelConfig> = {
  // Empty project config
};

export const calendarConfig: Partial<CalendarConfig> = {
  date: new Date(2022, 0, 1),
};
@Component({
  selector: 'app-prenotazioni',
  templateUrl: './prenotazioni.component.html',
  styleUrls: ['./prenotazioni.component.scss'],
})
export class PrenotazioniComponent {
  resources = [
    {
      id: 1,
      name: 'Default Calendar',
      eventColor: 'green',
    },
  ];

  events = [
    {
      id: 1,
      name: 'Meeting',
      startDate: '2022-01-01T10:00:00',
      endDate: '2022-01-01T11:00:00',
      resourceId: 1,
    },
  ];

  calendarConfig = calendarConfig;
  projectConfig = projectConfig;

  @ViewChild('calendar') calendarComponent!: BryntumCalendarComponent;
  @ViewChild('project') projectComponent!: BryntumProjectModelComponent;

  selected: Date | null = null;

  public data: object[] = [
    {
      Id: 1,
      Subject: 'Meeting',
      StartTime: new Date(2023, 1, 15, 10, 0),
      EndTime: new Date(2023, 1, 15, 12, 30),
    },
  ];
  public selectedDate: Date = new Date();
}
