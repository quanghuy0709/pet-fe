import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrenotazioniComponent } from './prenotazioni.component';

const routes: Routes = [
  {
    path: '',
    component: PrenotazioniComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrenotazioniRoutingModule {}
