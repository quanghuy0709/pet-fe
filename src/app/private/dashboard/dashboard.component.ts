import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Pet } from 'src/app/models/pet.model';
import { AuthService } from 'src/app/service/auth/auth.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { PetService } from 'src/app/service/pet/pet.service';
import { PetFormComponent } from '../pets/petForm/pet-form.component';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  user: any;
  myPets: Pet[] = [];
  showModalPetForm: boolean = false;

  currentPet?: Pet;

  constructor(
    private readonly localstorageService: LocalstorageService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly petService: PetService,
    private readonly matDialog: MatDialog,
    private readonly modalService: ModalService
  ) {}
  ngOnInit(): void {
    this.user = this.localstorageService.get('user');
    this.getPets();
  }

  getPets() {
    this.petService.getPets().subscribe({
      next: (value: Pet[]) => {
        this.myPets = value;
        if (this.myPets.length) {
          this.myPets[0].isSelected = true;
          this.currentPet = this.myPets[0];
        } else {
          this.addPet();
        }
      },
    });
  }

  addPet(currentPet?: Pet) {
    const dialogFormPet = this.matDialog.open(PetFormComponent, {
      // height: '70%',
      width: '100%',
      data: {
        pets: !!this.myPets.length,
        currentPet,
      },
      disableClose: !!!this.myPets.length,
    });
    dialogFormPet.afterClosed().subscribe({
      next: (value) => {
        this.getPets();
      },
    });
  }

  logout() {
    this.authService.logout().subscribe();
    this.localstorageService.remove('user');
    this.router.navigate(['']);
  }

  selectPet(currentPet: Pet) {
    this.currentPet = currentPet;
    this.myPets.map((pet) => {
      currentPet.isSelected = true;
      if (pet.id !== currentPet.id) {
        pet.isSelected = false;
      }
      return pet;
    });
  }

  onDeletePet(deletedPet: Pet) {
    this.petService.delete(deletedPet.id).subscribe({
      next: () => {
        this.getPets();
        this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
        this.router.navigate(['private', 'tedbuddy', 'dashboard']);
        },
      });
  }
}
