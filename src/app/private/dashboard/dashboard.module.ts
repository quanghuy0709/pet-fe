import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { GoogleMapsModule } from '@angular/google-maps';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DetailsDashboardComponent } from './details-dashboard/details-dashboard.component';

@NgModule({
  declarations: [DashboardComponent, DetailsDashboardComponent],
  imports: [CommonModule, DashboardRoutingModule, GoogleMapsModule, SharedModule],
})
export class DashboardModule {}
