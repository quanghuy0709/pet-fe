import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as luxon from 'luxon';
import { NearMarker, iLocation } from 'src/app/models/nearkMarker.model';
import { Pet } from 'src/app/models/pet.model';
import { GoogleMapService } from 'src/app/service/googleMap/googleMap.service';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';
import { PetService } from 'src/app/service/pet/pet.service';
import { iPet } from '../../../models/pet.model';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-details-dashboard',
  templateUrl: './details-dashboard.component.html',
  styleUrls: ['./details-dashboard.component.scss'],
})
export class DetailsDashboardComponent implements OnInit {
  @Input() pet?: Pet;
  @Output() editPet: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventDeletePet: EventEmitter<any> = new EventEmitter<any>();
  // nearMarkers: any[] = [];
  nearMarkersFromMe: NearMarker[] = [];
  idCardEdit: boolean = false;
  showButtonDelete: boolean = false;
  constructor(
    private readonly router: Router,
    private readonly googleMapService: GoogleMapService,
    private readonly healthcareService: HealthcareService,
    private readonly petService: PetService,
    private readonly modalService: ModalService
  ) {}

  ngOnInit(): void {
    this.healthcareService.getHealthCards({ pet: this.pet?.id }).subscribe(console.log);
    this.healthcareService.getMedicalHistories({ pet: this.pet?.id }).subscribe(console.log);
    this.healthcareService.getTreatmentPlans({ pet: this.pet?.id }).subscribe(console.log);
  }

  getCurrentPosition(currentPosition: iLocation) {
    this.googleMapService.search(currentPosition).subscribe({
      next: (nearMarkers) => {
        this.nearMarkersFromMe = nearMarkers;
        // this.nearMarkersFromMe.map((nearMarker) => this.markers.push(nearMarker));
      },
    });
  }

  get getYearOldPet() {
    var start: any = new Date(this.pet?.dateOfBirth!);
    const date1 = luxon.DateTime.fromISO(this.pet?.dateOfBirth!);
    const date2 = luxon.DateTime.now();

    const diff = date2.diff(date1, ['years', 'months', 'days', 'hours']);
    return diff;
  }

  openHelthcareModal() {
    this.router.navigate(['private', 'tedbuddy', 'healthcare'], { queryParams: { pet: this.pet?.id } });
  }

  mouserOver(isOverMouse: boolean = false) {
    this.idCardEdit = isOverMouse;
    this.showButtonDelete = isOverMouse;
  }

  editIdCard() {
    this.editPet.emit(this.pet);
  }

  showModalDeleteMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Sei sicuro?',
      message: 'Sei sicuro di voler eliminare il tuo pet?',
      visible: true,
    };
  }

  closeModal() {
    this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
  }

  deletePet() {
    if (this.pet) {
      this.petService.delete(this.pet.id).subscribe({
        next: () => {
          this.closeModal();
          this.eventDeletePet.emit(true);
        },
      });
    }
  }
}
