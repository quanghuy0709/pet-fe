import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PrivateComponent } from './private.component';
import { professionalGuard, tedBuddyGuard } from '../guards/professional-or-ted-buddy.guard';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    children: [
      {
        path: 'tedbuddy',
        loadChildren: () => import('./tedbuddy/tedbuddy.module').then((m) => m.TedbuddyModule),
        canActivate: [tedBuddyGuard],
      },
      {
        path: 'professional',
        loadChildren: () => import('./professional/professional.module').then((m) => m.ProfessionalModule),
        canActivate: [professionalGuard],
      },
      // {
      //   path: 'dashboard',
      //   loadChildren: () =>
      //     import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      // },
      // {
      //   path: 'dashboard-professional',
      //   loadChildren: () =>
      //     import('./dashboard-professional/dashboard-professional.module').then(
      //       (m) => m.DashboardProfessionalModule
      //     ),
      // },
      // {
      //   path: 'note',
      //   loadChildren: () =>
      //     import('./notes/notes.module').then((m) => m.NotesModule),
      // },
      // {
      //   path: 'messaggi',
      //   loadChildren: () =>
      //     import('./messaggi/messaggi.module').then((m) => m.MessaggiModule),
      // },
      // {
      //   path: 'prenotazioni',
      //   loadChildren: () =>
      //     import('./prenotazioni/prenotazioni.module').then(
      //       (m) => m.PrenotazioniModule
      //     ),
      // },
      // {
      //   path: 'nuts',
      //   loadChildren: () =>
      //     import('./nuts/nuts.module').then((m) => m.NutsModule),
      // },
      // {
      //   path: 'pets',
      //   loadChildren: () =>
      //     import('./pets/pets.module').then((m) => m.PetsModule),
      // },
      // {
      //   path: 'profile',
      //   loadChildren: () =>
      //     import('./profile/profile.module').then((m) => m.ProfileModule),
      // },
      {
        path: '',
        redirectTo: 'tedbuddy',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}
