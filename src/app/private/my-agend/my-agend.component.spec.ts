import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAgendComponent } from './my-agend.component';

describe('MyAgendComponent', () => {
  let component: MyAgendComponent;
  let fixture: ComponentFixture<MyAgendComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MyAgendComponent]
    });
    fixture = TestBed.createComponent(MyAgendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
