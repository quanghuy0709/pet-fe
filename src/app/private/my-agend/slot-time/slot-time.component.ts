import { Component, Input } from '@angular/core';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-slot-time',
  templateUrl: './slot-time.component.html',
  styleUrls: ['./slot-time.component.scss'],
})
export class SlotTimeComponent {
  @Input() titleDay: string = 'Lunedì';
  hoursForSelect: any[] = [];
  Timetables: any[] = [];
  arrayOfTimetables: any[] = [];
  numberOfTimeTables = 0;

  constructor() {
    for (let hour = 0; hour < 24; hour++) {
      this.hoursForSelect.push(DateTime.fromObject({ hour }).toFormat('HH:mm'));
      this.hoursForSelect.push(DateTime.fromObject({ hour, minute: 30 }).toFormat('HH:mm'));
    }
  }
  addSlot() {
    this.numberOfTimeTables++;
    this.Timetables.push(this.numberOfTimeTables);
    this.arrayOfTimetables.push({ id: this.numberOfTimeTables, startTime: this.hoursForSelect[0], endTime: this.hoursForSelect[0] });
  }

  removeSlot(id: number) {
    const index = this.arrayOfTimetables.findIndex((value) => value.id == id);
    if (index > -1) this.arrayOfTimetables.splice(index, 1);
  }

  fromTime(event: any, id: number) {
    this.arrayOfTimetables.find((timeTable) => timeTable.id == id).startTime = event.target.value;
  }

  toTime(event: any, id: number) {
    this.arrayOfTimetables.find((timeTable) => timeTable.id == id).endTime = event.target.value;
  }
}
