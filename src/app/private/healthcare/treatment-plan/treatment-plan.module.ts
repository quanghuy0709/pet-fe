import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreatmentPlanRoutingModule } from './treatment-plan-routing.module';
import { TreatmentPlanComponent } from './treatment-plan.component';
import { FormTreatmentPlanComponent } from './form-treatment-plan/form-treatment-plan.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from 'src/app/shared/shared.module';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { BryntumCalendarModule } from '@bryntum/calendar-angular';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';

@NgModule({
  declarations: [TreatmentPlanComponent, FormTreatmentPlanComponent],
  imports: [
    CommonModule,
    TreatmentPlanRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CdkAccordionModule,
    BryntumCalendarModule,
    NgxMatTimepickerModule,
  ],
  exports: [TreatmentPlanComponent],
})
export class TreatmentPlanModule {}
