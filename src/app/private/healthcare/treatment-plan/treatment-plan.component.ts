import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Scheduler } from '@bryntum/calendar';
import { BryntumCalendarComponent } from '@bryntum/calendar-angular';
import { DateTime } from 'luxon';
import { map } from 'rxjs/operators';
import { TreatmentPlan } from 'src/app/models/treatmentPlan.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';

@Component({
  selector: 'app-treatment-plan',
  templateUrl: './treatment-plan.component.html',
  styleUrls: ['./treatment-plan.component.scss'],
})
export class TreatmentPlanComponent implements OnInit {
  @ViewChild('calendar') calendar?: BryntumCalendarComponent;
  treatmentPlans: any[] = [];
  showModalCreate = false;
  currentTreatmentPlan?: TreatmentPlan;
  idPet: string = '';

  constructor(private readonly healthcareService: HealthcareService, private readonly activatedRoute: ActivatedRoute) {}
  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.getTreatmentsPlan().subscribe();
  }
  openFormModal() {
    this.showModalCreate = !this.showModalCreate;
  }

  getTreatmentsPlan() {
    return this.healthcareService.getTreatmentPlans({ pet: this.idPet }).pipe(
      map((values) => {
        values.map((item) => {
          if (item.times.length) {
            item.schedules.map((schedule) => {
              this.treatmentPlans.push({
                id: schedule._id,
                name: item.name,
                startDate: DateTime.fromISO(new Date(schedule.dateStart).toISOString()).toISO(),
                endDate: DateTime.fromISO(new Date(schedule.dateEnd).toISOString()).toISO(),
                resourceId: schedule._id,
              });
            });
          } else {
            return;
          }
        });
      })
    );
  }

  closeModal(event: any) {
    this.showModalCreate = event.showModal;
    if (event.isReload) {
      this.getTreatmentsPlan().subscribe();
    }
  }

  editCurrentHealthCard(treatmentPlan: TreatmentPlan) {
    this.currentTreatmentPlan = treatmentPlan;
    this.openFormModal();
  }
}
