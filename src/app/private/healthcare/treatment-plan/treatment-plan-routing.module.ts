import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TreatmentPlanComponent } from './treatment-plan.component';
import { FormTreatmentPlanComponent } from './form-treatment-plan/form-treatment-plan.component';

const routes: Routes = [
  {
    path: '',
    component: TreatmentPlanComponent,
  },
  {
    path: 'create',
    component: FormTreatmentPlanComponent,
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TreatmentPlanRoutingModule {}
