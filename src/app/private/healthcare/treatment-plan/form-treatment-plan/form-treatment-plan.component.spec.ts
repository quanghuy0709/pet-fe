import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTreatmentPlanComponent } from './form-treatment-plan.component';

describe('FormTreatmentPlanComponent', () => {
  let component: FormTreatmentPlanComponent;
  let fixture: ComponentFixture<FormTreatmentPlanComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormTreatmentPlanComponent]
    });
    fixture = TestBed.createComponent(FormTreatmentPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
