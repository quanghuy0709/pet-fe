import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TreatmentPlan } from 'src/app/models/treatmentPlan.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';

@Component({
  selector: 'app-form-treatment-plan',
  templateUrl: './form-treatment-plan.component.html',
  styleUrls: ['./form-treatment-plan.component.scss'],
})
export class FormTreatmentPlanComponent implements OnInit, OnChanges {
  @Input() showModal = false;
  @Input() treamtmentPlanToEdit?: TreatmentPlan;

  @Output() closeModalAction = new EventEmitter<any>();
  idPet: string = '';
  typeForm?: 'vaccine' | 'antiprasitic';
  addExtraTreatmentForm = false;
  myForm?: FormGroup;
  manyRepeatTimeSlot: any[] = new Array(1);

  timesArrayRepeat: any[] = [];
  typeHealthcards: any[] = [
    {
      title: 'Vaccini',
      icon: 'assets/sprinted/healthcare/vaccine_icon.svg',
      type: 'vaccine',
    },
    {
      title: 'Antiparassitari',
      icon: 'assets/sprinted/healthcare/antiparasitic_icon.svg',
      type: 'antiprasitic',
    },
  ];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly healthcareService: HealthcareService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.treamtmentPlanToEdit) {
      this.myForm?.patchValue(this.treamtmentPlanToEdit);
    }
  }

  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.myForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      drugName: ['', [Validators.required]],
      methodAdministration: [null],
      quantity: [''],
      startDate: [''],
      endDate: [''],
      notification: [false],
      frequency: [null],
      // dosage: [''],
      numberToRepeat: ['1'],
      ifRepeats: [false],
      times: [[]],
      pet: [this.idPet],
    });
  }

  closeModal(reload = false) {
    this.showModal = !this.showModal;
    this.addExtraTreatmentForm = false;
    this.typeForm = undefined;
    this.treamtmentPlanToEdit = undefined;
    this.closeModalAction.emit({ showModal: this.showModal, isReload: reload });
  }

  selectForm(type: any) {
    this.typeForm = type;
    this.myForm?.controls['type'].patchValue(type);
  }
  turnBackToSelect() {
    this.addExtraTreatmentForm = !this.addExtraTreatmentForm;
    this.typeForm = undefined;
  }

  addExtraRecall() {
    this.addExtraTreatmentForm = !this.addExtraTreatmentForm;
  }

  addHealtcard(form: FormGroup) {
    form.controls['times'].setValue([...this.timesArrayRepeat]);
    if (form.valid) {
      this.healthcareService.createTreatmentPlan(form.value).subscribe({
        next: (response) => {
          form.reset();
          this.closeModal(true);
        },
        error: (error) => {
          form.reset();
          this.closeModal(false);
        },
      });
    }
  }

  howManyCount(event: any) {
    const numberToRepeat = event;
    this.manyRepeatTimeSlot = new Array(Number(numberToRepeat ?? 0));
  }

  onTimeChanged(event: any, index: number) {
    this.timesArrayRepeat[index] = event;
  }
}
