import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CODE, CodeGeneral } from 'src/app/models/codeGeneral.model';
import { MedicalHistory } from 'src/app/models/medicalHistory.model';
import { CodesService } from 'src/app/service/codes/codes.service';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';

@Component({
  selector: 'app-form-medical-history',
  templateUrl: './form-medical-history.component.html',
  styleUrls: ['./form-medical-history.component.scss'],
})
export class FormMedicalHistoryComponent implements OnInit, OnChanges {
  @Input() showModal = false;
  @Input() medicalHistoryToEdit?: MedicalHistory;
  @Output() closeModalAction = new EventEmitter<any>();
  idPet: string = '';
  typeForm?: 'medical-examination' | 'prescription';
  showForm = false;
  optionsTypeVisits: CodeGeneral[] = [];
  addExtraRecallForm = false;
  currentItemCode: string = '';
  myForm?: FormGroup;
  typeHealthcards: any[] = [
    {
      title: 'Visite mediche',
      icon: 'assets/sprinted/healthcare/medical-examination_icon.svg',
      type: 'medical-examination',
    },
    {
      title: 'Ricetta',
      icon: 'assets/sprinted/healthcare/prescription_icon.svg',
      type: 'prescription',
    },
  ];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly healthcareService: HealthcareService,
    private readonly codeService: CodesService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.medicalHistoryToEdit) {
      this.selectForm(this.medicalHistoryToEdit.type);
      if (this.medicalHistoryToEdit.type === 'medical-examination') {
        this.myForm?.patchValue({
          vetClinical: this.medicalHistoryToEdit.detail?.medicalExamination?.vetClinic,
          type: this.medicalHistoryToEdit.type,
          vet: this.medicalHistoryToEdit.detail?.medicalExamination?.vet,
          typeOfVisit: this.medicalHistoryToEdit.detail?.medicalExamination?.typeOfVisit,
          whenApplied: this.medicalHistoryToEdit.detail?.medicalExamination?.whenApplied,
        });
      } else {
        this.myForm?.patchValue({
          type: this.medicalHistoryToEdit.type,
          vet: this.medicalHistoryToEdit.detail?.prescription?.vet,
          whenApplied: this.medicalHistoryToEdit.detail?.prescription?.whenApplied,
          tipologyOfPrescription: this.medicalHistoryToEdit.detail?.prescription?.tipologyOfPrescription,
          nrPrescription: this.medicalHistoryToEdit.detail?.prescription?.nrPrescription,
          pin: this.medicalHistoryToEdit.detail?.prescription?.pin,
        });
      }
    }
  }

  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.myForm = this.formBuilder.group({
      vetClinical: ['', []],
      type: [this.typeForm],
      vet: [''],
      typeOfVisit: [''],
      whenApplied: [''],
      tipologyOfPrescription: [null],
      nrPrescription: [''],
      pin: [''],

      note: [''],
      pet: [this.idPet],
    });
  }

  closeModal(reload = false) {
    this.showModal = !this.showModal;
    this.showForm = false;
    this.addExtraRecallForm = false;
    this.typeForm = undefined;
    this.medicalHistoryToEdit = undefined;
    this.closeModalAction.emit({ showModal: this.showModal, isReload: reload });
  }

  selectForm(type: any) {
    this.showForm = !this.showForm;
    this.typeForm = type;
    this.myForm?.controls['type'].patchValue(type);
  }
  turnBackToSelect() {
    this.showForm = !this.showForm;
    this.addExtraRecallForm = !this.addExtraRecallForm;
    this.typeForm = undefined;
  }

  addExtraRecall() {
    this.addExtraRecallForm = !this.addExtraRecallForm;
  }

  addHealtcard(form: FormGroup) {
    this.transformBodyForType(form.controls['type'].value, form);
  }

  transformBodyForType(type: 'medical-examination' | 'prescription', form: FormGroup) {
    if (form.valid) {
      let body = {};
      if (type === 'medical-examination') {
        body = {
          type: 'medical-examination',
          detail: {
            medicalExamination: {
              typeOfVisit: form.controls['typeOfVisit'].value,
              vet: form.controls['vet'].value,
              vetClinic: form.controls['vetClinical'].value,
              whenApplied: form.controls['whenApplied'].value,
              note: form.controls['note'].value,
            },
          },
          pet: form.controls['pet'].value,
        };
      } else {
        body = {
          type: 'prescription',
          detail: {
            prescription: {
              tipologyOfPrescription: form.controls['tipologyOfPrescription'].value,
              vet: form.controls['vet'].value,
              nrPrescription: form.controls['nrPrescription'].value,
              pin: form.controls['pin'].value,
              whenApplied: form.controls['whenApplied'].value,
              note: form.controls['note'].value,
            },
          },
          pet: '64c68807148c92576b7b6ad2',
        };
      }
      this.healthcareService.createMedicalHistory(body).subscribe({
        next: (response) => {
          form.reset();
          this.closeModal(true);
        },
        error: (error) => {
          form.reset();
          this.closeModal();
        },
      });
    }
  }

  searchByCode(event: any) {
    const searchString = event.target.value;
    this.codeService.searchBycode(this.currentItemCode, searchString, CODE.TypologyPrescription).subscribe({
      next: (items) => {
        this.optionsTypeVisits = items;
      },
    });
  }
}
