import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormMedicalHistoryComponent } from './form-medical-history.component';

describe('FormMedicalHistoryComponent', () => {
  let component: FormMedicalHistoryComponent;
  let fixture: ComponentFixture<FormMedicalHistoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormMedicalHistoryComponent]
    });
    fixture = TestBed.createComponent(FormMedicalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
