import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { MedicalHistory } from 'src/app/models/medicalHistory.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';

@Component({
  selector: 'app-medical-history',
  templateUrl: './medical-history.component.html',
  styleUrls: ['./medical-history.component.scss'],
})
export class MedicalHistoryComponent implements OnInit, OnChanges {
  medicalHistories: MedicalHistory[] = [];
  showModalCreate = false;
  currentMedicalHistory?: MedicalHistory;
  idPet: string = '';

  constructor(private readonly healthcareService: HealthcareService, private readonly activatedRoute: ActivatedRoute) {}
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.getMedicalHistories().subscribe();
  }

  getMedicalHistories() {
    return this.healthcareService.getMedicalHistories({ pet: this.idPet }).pipe(map((value) => (this.medicalHistories = value)));
  }

  openFormModal() {
    this.showModalCreate = !this.showModalCreate;
  }

  closeFormModal(event: any) {
    this.showModalCreate = event.showModal;
    if (event.isReload) {
      this.getMedicalHistories().subscribe();
    }
  }

  editCurrentMedicalHistory(medicalhistory: MedicalHistory) {
    this.currentMedicalHistory = medicalhistory;
    this.openFormModal();
  }
}
