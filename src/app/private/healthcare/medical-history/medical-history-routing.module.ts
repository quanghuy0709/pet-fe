import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MedicalHistoryComponent } from './medical-history.component';
import { FormMedicalHistoryComponent } from './form-medical-history/form-medical-history.component';

const routes: Routes = [
  {
    path: '',
    component: MedicalHistoryComponent,
  },
  {
    path: 'create',
    component: FormMedicalHistoryComponent,
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalHistoryRoutingModule {}
