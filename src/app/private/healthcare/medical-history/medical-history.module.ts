import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicalHistoryRoutingModule } from './medical-history-routing.module';
import { MedicalHistoryComponent } from './medical-history.component';
import { FormMedicalHistoryComponent } from './form-medical-history/form-medical-history.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CdkAccordionModule } from '@angular/cdk/accordion';

@NgModule({
  declarations: [MedicalHistoryComponent, FormMedicalHistoryComponent],
  imports: [
    CommonModule,
    MedicalHistoryRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SharedModule,
    CdkAccordionModule,
  ],
  exports: [MedicalHistoryComponent],
})
export class MedicalHistoryModule {}
