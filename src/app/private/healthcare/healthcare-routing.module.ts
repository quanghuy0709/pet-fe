import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthcareComponent } from './healthcare.component';

const routes: Routes = [
  {
    path: '',
    component: HealthcareComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HealthcareRoutingModule {}
