import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs';
import { HealthCard } from 'src/app/models/healthCard.model';
import { MedicalHistory } from 'src/app/models/medicalHistory.model';
import { Pet } from 'src/app/models/pet.model';
import { TreatmentPlan } from 'src/app/models/treatmentPlan.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';
import { PetService } from 'src/app/service/pet/pet.service';

@Component({
  selector: 'app-healthcare',
  templateUrl: './healthcare.component.html',
  styleUrls: ['./healthcare.component.scss'],
})
export class HealthcareComponent implements OnInit {
  healthCards: HealthCard[] = [];
  medicalHistories: MedicalHistory[] = [];
  treatmentPlans: TreatmentPlan[] = [];
  activeIndex = 0;
  pet?: Pet;

  constructor(
    private readonly router: Router,
    private readonly healthcareService: HealthcareService,
    private readonly petService: PetService,
    private readonly activatedRoute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.activatedRoute.queryParams
      .pipe(
        switchMap((param: any) => {
          return this.petService.getPet(param?.pet);
        })
      )
      .subscribe({
        next: (pet) => {
          this.pet = pet;
        },
      });
  }
  closeModal() {
    this.router.navigate(['..']);
  }
}
