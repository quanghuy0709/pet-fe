import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthcardComponent } from './healthcard.component';

describe('HealthcardComponent', () => {
  let component: HealthcardComponent;
  let fixture: ComponentFixture<HealthcardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HealthcardComponent]
    });
    fixture = TestBed.createComponent(HealthcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
