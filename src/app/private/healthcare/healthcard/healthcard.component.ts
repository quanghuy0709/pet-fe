import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { HealthCard } from 'src/app/models/healthCard.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';
import { PdfService } from 'src/app/service/pdf/pdf.service';

@Component({
  selector: 'app-healthcard',
  templateUrl: './healthcard.component.html',
  styleUrls: ['./healthcard.component.scss'],
})
export class HealthcardComponent implements OnInit {
  healthCards: HealthCard[] = [];
  showModalCreate = false;
  currentHealthCard?: HealthCard;
  isOpenAccordion = false;
  idPet: string = '';
  isEdit: boolean = false;
  isPdfLoading: boolean = false;

  constructor(
    private readonly healthcareService: HealthcareService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly pdfService: PdfService
  ) {}
  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.getHealthcards().subscribe();
  }

  getHealthcards() {
    return this.healthcareService.getHealthCards({ pet: this.idPet }).pipe(map((healthCardsResponse) => (this.healthCards = healthCardsResponse)));
  }

  openFormModal() {
    this.showModalCreate = !this.showModalCreate;
  }

  closeFormModal(event: any) {
    this.isEdit = false;
    this.showModalCreate = event.showModal;
    if (event.isReload) {
      this.getHealthcards().subscribe();
    }
  }

  editCurrentHealthCard(healthCard: HealthCard) {
    this.currentHealthCard = healthCard;
    this.isEdit = !this.isEdit;
    this.openFormModal();
  }

  openAccordion(event: any) {
    console.log(event);
  }

  donwloadPdfHealthCard() {
    this.isPdfLoading = true;
    this.pdfService.downloadPdf(this.idPet).subscribe({
      next: (value) => {
        this.isPdfLoading = false;
      },
      error: (error) => {
        this.isPdfLoading = false;
        console.error('ERROR', error);
      },
    });
  }
}
