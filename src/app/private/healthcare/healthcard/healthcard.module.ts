import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HealthcardRoutingModule } from './healthcard-routing.module';
import { HealthcardComponent } from './healthcard.component';
import { FormHealthcardComponent } from './form-healthcard/form-healthcard.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CdkAccordionModule } from '@angular/cdk/accordion';

@NgModule({
  declarations: [HealthcardComponent, FormHealthcardComponent],
  imports: [
    CommonModule,
    HealthcardRoutingModule,
    SharedModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormsModule,
    CdkAccordionModule,
    SharedModule,
  ],
  exports: [HealthcardComponent],
})
export class HealthcardModule {}
