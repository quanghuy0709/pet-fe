import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HealthCard } from 'src/app/models/healthCard.model';
import { HealthcareService } from 'src/app/service/healthcare/healthcare.service';

@Component({
  selector: 'app-form-healthcard',
  templateUrl: './form-healthcard.component.html',
  styleUrls: ['./form-healthcard.component.scss'],
})
export class FormHealthcardComponent implements OnInit, OnChanges {
  @Input() showModal = false;
  @Input() isEdit = false;
  @Input() healthCardToEdit?: HealthCard;
  @Output() closeModalAction = new EventEmitter<any>();
  idPet: string = '';
  typeForm?: 'vaccine' | 'antiprasitic';
  showForm = false;
  addExtraRecallForm = false;
  myForm?: FormGroup;
  typeHealthcards: any[] = [
    {
      title: 'Vaccini',
      icon: 'assets/sprinted/healthcare/vaccine_icon.svg',
      type: 'vaccine',
    },
    {
      title: 'Antiparassitari',
      icon: 'assets/sprinted/healthcare/antiparasitic_icon.svg',
      type: 'antiparasitic',
    },
  ];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly healthcareService: HealthcareService
  ) {}
  ngOnChanges(changes: SimpleChanges): void {
    if (this.healthCardToEdit && this.isEdit) {
      this.selectForm(this.healthCardToEdit.type);
      if (this.healthCardToEdit.recall?.dateOfRecall) {
        this.addExtraRecallForm = true;
      }
      this.myForm?.patchValue({
        name: this.healthCardToEdit.name,
        vet: this.healthCardToEdit.veterinarian,
        whenApplied: this.healthCardToEdit.whenApplied,
        note: this.healthCardToEdit.note,
        lot: this.healthCardToEdit.lot,
        recall: this.healthCardToEdit.recall,
        pet: this.healthCardToEdit.pet,
        remember: this.addExtraRecallForm,
      });
    }
  }

  ngOnInit(): void {
    this.idPet = this.activatedRoute.snapshot.queryParamMap.get('pet') ?? '';
    this.myForm = this.formBuilder.group({
      lot: ['', []],
      name: ['', [Validators.required]],
      type: [this.typeForm],
      vet: [''],
      whenApplied: [''],
      note: [''],
      recall: this.formBuilder.group({
        dateOfRecall: [''],
      }),
      pet: [this.idPet],
      remember: [false],
    });
  }

  closeModal(reload = false) {
    this.showModal = !this.showModal;
    this.showForm = false;
    this.addExtraRecallForm = false;
    this.typeForm = undefined;
    this.myForm?.reset();
    // this.healthCardToEdit = undefined;
    this.closeModalAction.emit({ showModal: this.showModal, isReload: reload });
  }

  selectForm(type: any) {
    this.showForm = !this.showForm;
    this.typeForm = type;
    this.myForm?.controls['type'].patchValue(type);
  }
  turnBackToSelect() {
    this.showForm = !this.showForm;
    this.addExtraRecallForm = !this.addExtraRecallForm;
    this.typeForm = undefined;
  }

  addExtraRecall() {
    this.addExtraRecallForm = !this.addExtraRecallForm;
    this.myForm?.controls['remember'].patchValue(this.addExtraRecall);
  }

  addHealtcard(form: FormGroup) {
    form.removeControl('remember');
    if (form.valid) {
      this.healthcareService.manageHealthCard(this.isEdit ? 'edit' : 'add', this.healthCardToEdit?.id, form.value).subscribe({
        next: (response) => {
          form.reset();
          this.closeModal(true);
        },
        error: (error) => {
          form.reset();
          this.closeModal();
        },
      });
    }
  }
}
