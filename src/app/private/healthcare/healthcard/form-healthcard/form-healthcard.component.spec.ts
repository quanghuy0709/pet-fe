import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHealthcardComponent } from './form-healthcard.component';

describe('FormHealthcardComponent', () => {
  let component: FormHealthcardComponent;
  let fixture: ComponentFixture<FormHealthcardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormHealthcardComponent],
    });
    fixture = TestBed.createComponent(FormHealthcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
