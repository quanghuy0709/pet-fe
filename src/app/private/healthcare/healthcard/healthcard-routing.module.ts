import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HealthcardComponent } from './healthcard.component';
import { FormHealthcardComponent } from './form-healthcard/form-healthcard.component';

const routes: Routes = [
  {
    path: "", 
    component: HealthcardComponent
  },
  {
    path: "create", 
    component: FormHealthcardComponent
  },
  {
    path: "", 
    redirectTo :"",
    pathMatch: "full"
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthcardRoutingModule { }
