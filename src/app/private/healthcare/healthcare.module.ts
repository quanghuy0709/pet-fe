import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HealthcardModule } from './healthcard/healthcard.module';
import { HealthcareRoutingModule } from './healthcare-routing.module';
import { HealthcareComponent } from './healthcare.component';
import { MedicalHistoryModule } from './medical-history/medical-history.module';
import { TreatmentPlanModule } from './treatment-plan/treatment-plan.module';

import { MatTabsModule } from '@angular/material/tabs';
import { CdkAccordionModule } from '@angular/cdk/accordion';

@NgModule({
  declarations: [HealthcareComponent],
  imports: [CommonModule, HealthcareRoutingModule, HealthcardModule, MedicalHistoryModule, TreatmentPlanModule, MatTabsModule, CdkAccordionModule],
})
export class HealthcareModule {}
