import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardProfessionalRoutingModule } from './dashboard-professional-routing.module';
import { DashboardProfessionalComponent } from './dashboard-professional.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BryntumCalendarModule } from '@bryntum/calendar-angular';

@NgModule({
  declarations: [DashboardProfessionalComponent],
  imports: [CommonModule, DashboardProfessionalRoutingModule,  SharedModule, BryntumCalendarModule],
})
export class DashboardProfessionalModule {}
