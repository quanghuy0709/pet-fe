import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CalendarConfig, ProjectModelConfig } from '@bryntum/calendar';
import { BryntumCalendarComponent, BryntumProjectModelComponent } from '@bryntum/calendar-angular';
import { AuthService } from 'src/app/service/auth/auth.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';

export const projectConfig: Partial<ProjectModelConfig> = {
  // Empty project config
};

export const calendarConfig: Partial<CalendarConfig> = {
  date: new Date(2022, 0, 1),
};

@Component({
  selector: 'app-dashboard-professional',
  templateUrl: './dashboard-professional.component.html',
  styleUrls: ['./dashboard-professional.component.scss'],
  providers: [],
})
export class DashboardProfessionalComponent {
  resources = [
    {
      id: 1,
      name: 'Default Calendar',
      eventColor: 'green',
    },
  ];

  events = [
    {
      id: 1,
      name: 'Meeting',
      startDate: '2022-01-01T10:00:00',
      endDate: '2022-01-01T11:00:00',
      resourceId: 1,
    },
  ];

  calendarConfig = calendarConfig;
  projectConfig = projectConfig;

  @ViewChild('calendar') calendarComponent!: BryntumCalendarComponent;
  @ViewChild('project') projectComponent!: BryntumProjectModelComponent;

  selected: Date | null = null;

  public data: object[] = [
    {
      Id: 1,
      Subject: 'Meeting',
      StartTime: new Date(2023, 1, 15, 10, 0),
      EndTime: new Date(2023, 1, 15, 12, 30),
    },
  ];
  public selectedDate: Date = new Date();

  constructor(
    private readonly localstorageService: LocalstorageService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  logout() {
    this.authService.logout().subscribe();
    this.localstorageService.remove('user');
    this.router.navigate(['']);
  }
}
