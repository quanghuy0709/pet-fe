import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardProfessionalComponent } from './dashboard-professional.component';
import { MyAgendComponent } from '../my-agend/my-agend.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardProfessionalComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardProfessionalRoutingModule {}
