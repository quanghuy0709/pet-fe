import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth/auth.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-professional',
  templateUrl: './professional.component.html',
  styleUrls: ['./professional.component.scss'],
})
export class ProfessionalComponent {
  constructor(
    private readonly localstorageService: LocalstorageService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  logout() {
    this.authService.logout().subscribe();
    this.localstorageService.remove('user');
    this.router.navigate(['']);
  }
}
