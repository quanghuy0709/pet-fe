import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyAgendComponent } from '../my-agend/my-agend.component';
import { ProfessionalComponent } from './professional.component';
import { ProfileProfessionalComponent } from '../profile-professional/profile-professional.component';

const routes: Routes = [
  {
    path: '',
    component: ProfessionalComponent,
    children: [
      {
        path: 'dashboard-professional',
        loadChildren: () => import('../dashboard-professional/dashboard-professional.module').then((m) => m.DashboardProfessionalModule),
      },
      {
        path: 'my_agend',
        component: MyAgendComponent,
      },
      {
        path: 'profile',
        component: ProfileProfessionalComponent,
      },
      {
        path: '',
        redirectTo: 'dashboard-professional',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfessionalRoutingModule {}
