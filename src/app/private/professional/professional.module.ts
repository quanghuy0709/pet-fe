import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessionalRoutingModule } from './professional-routing.module';
import { ProfessionalComponent } from './professional.component';
import { MyAgendComponent } from '../my-agend/my-agend.component';
import { SlotTimeComponent } from '../my-agend/slot-time/slot-time.component';

@NgModule({
  declarations: [ProfessionalComponent, MyAgendComponent, SlotTimeComponent],
  imports: [CommonModule, ProfessionalRoutingModule],
})
export class ProfessionalModule {}
