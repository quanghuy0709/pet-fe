import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: any = [];
  user: any;
  userAdmin: boolean = false;
  displayedColumns: string[] = ['username', 'name', 'surname', 'role', 'kind', 'actions'];
  constructor(private userService: UserService, private readonly localstorageService: LocalstorageService) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe((resp: any) => {
      this.users = resp.data;
    });
    this.user = this.localstorageService.get('user');
    console.log(this.user.data.role);
  }
}
