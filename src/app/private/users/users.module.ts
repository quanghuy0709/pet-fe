import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [UsersComponent],
  imports: [CommonModule, UsersRoutingModule, MatCardModule, MatTableModule, MatPaginatorModule, SharedModule],
  exports: [UsersComponent],
})
export class UsersModule {}
