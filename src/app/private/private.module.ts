import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GoogleMapsModule } from '@angular/google-maps';
import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { ProfileProfessionalModule } from './profile-professional/profile-professional.module';

@NgModule({
  declarations: [PrivateComponent],
  imports: [CommonModule, PrivateRoutingModule, GoogleMapsModule, ProfileProfessionalModule],
})
export class PrivateModule {}
