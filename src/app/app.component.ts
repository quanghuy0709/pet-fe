import { Component, OnInit } from '@angular/core';
import 'flowbite';
import { initFlowbite } from 'flowbite';
import { ScriptJsLoaderService } from './service/script-js-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    initFlowbite();
  }
  constructor(private scriptJsLoaderService: ScriptJsLoaderService) {
    this.scriptJsLoaderService
      .load('mainjs')
      .then((data) => {
        console.log('script loaded ', data);
      })
      .catch((error) => console.error(error));
  }

  title = 'sprinted';
}
