import { AfterViewInit, Component, ElementRef, EventEmitter, Input, NgZone, Output, ViewChild } from '@angular/core';
import { GoogleMap } from '@angular/google-maps';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-search-map',
  templateUrl: './search-map.component.html',
  styleUrls: ['./search-map.component.scss'],
})
export class SearchMapComponent implements AfterViewInit {
  @Output() onAddressSelected = new EventEmitter<any>();
  @Input() isInlineInput = false;
  @Input() showLabel = false;
  @Input() typeInput = 'text';
  @Input() placeholderText = 'Indirizzo di residenza';
  @Input() classLabel =
    "before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 before:rounded-tl-md before:border-t before:border-l before:border-sprinted-orange before:transition-all after:pointer-events-none after:mt-[6.5px] after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r after:border-sprinted-orange after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-sprinted-orange peer-focus:before:border-t-2 peer-focus:before:border-l-2 peer-focus:before:border-sprinted-orange peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-sprinted-orange peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500";
  @Input() classInput =
    'peer h-full w-full rounded-[7px] border border-sprinted-orange bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-sprinted-orange placeholder-shown:border-t-sprinted-orange focus:border-2 focus:border-sprinted-orange focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50 ring-transparent focus:ring-transparent';
  @ViewChild('search')
  public searchElementRef!: ElementRef;
  @ViewChild(GoogleMap)
  public map!: GoogleMap;
  user: any;
  valueAddress?: string;

  constructor(private ngZone: NgZone, private readonly localstorageService: LocalstorageService) {
    this.user = localstorageService.get('user');
    this.valueAddress = this.user?.data?.address?.formattedAddress ?? '';
  }

  ngAfterViewInit(): void {
    let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
    autocomplete.addListener('place_changed', () => {
      this.ngZone.run(() => {
        //get the place result
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        //verify result
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        const latitude = place.geometry.location?.lat();
        const longitude = place.geometry.location?.lng();
        const address = {
          formattedAddress: place.formatted_address,
          location: {
            lat: latitude,
            lng: longitude,
          },
        };
        this.onAddressSelected.emit(address);
      });
    });
  }
}
