import { Component } from '@angular/core';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  constructor(private readonly modalService: ModalService) {}
  ariaHidden: boolean = true;

  showModal(type: 'login' | 'registrati', mode: 'tedbuddy' | 'professional' = 'tedbuddy') {
    if (type == 'login') {
      this.modalService.showLogin = true;
    } else {
      this.modalService.showRegisterComponent = true;
      this.modalService.modeRegister = mode;
    }
  }

  openDropdown() {
    this.ariaHidden = !this.ariaHidden;
  }
}
