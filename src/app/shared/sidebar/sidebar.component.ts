import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth/auth.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent {
  constructor(
    private readonly localstorageService: LocalstorageService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public get user(): any {
    return this.localstorageService.get('user');
  }

  logout() {
    this.authService.logout().subscribe();
    this.localstorageService.remove('user');
    this.router.navigate(['']);
  }

  goToProfile() {
    this.router.navigate(['private', 'tedbuddy', 'profile']);
  }

  navigateToHome() {
    this.router.navigate(['/'], { state: { isLogged: true } });
  }
}
