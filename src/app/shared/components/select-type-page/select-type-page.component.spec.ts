import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTypePageComponent } from './select-type-page.component';

describe('SelectTypePageComponent', () => {
  let component: SelectTypePageComponent;
  let fixture: ComponentFixture<SelectTypePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelectTypePageComponent]
    });
    fixture = TestBed.createComponent(SelectTypePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
