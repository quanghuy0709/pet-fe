import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-select-type-page',
  templateUrl: './select-type-page.component.html',
  styleUrls: ['./select-type-page.component.scss'],
})
export class SelectTypePageComponent {
  @Input() showModal: boolean = false;
  @Input() items: any[] = [];
  @Output() selectedTypeValue: EventEmitter<string> = new EventEmitter<string>();
}
