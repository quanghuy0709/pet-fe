import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent {
  @Input() showModal = false;
  modalImage?: string;
  modalTitle?: string;
  modalMessage?: string;
  constructor(private readonly modalService: ModalService) {
    // Suscribirse al Observable showMessage para controlar la visibilidad del modal
    // this.modalService.showMessage.subscribe((messageData) => {
    //   // Actualizar el valor de showModal según la visibilidad del mensaje
    //   this.showModal = messageData.visible;
    // });
    this.modalService.showMessage.subscribe({
      next: (showMessage: any) => {
        this.showModal = showMessage.visible;
        this.modalImage = '/assets/modal/modal-' + showMessage.type + '.jpg';
        this.modalTitle = showMessage.title;
        this.modalMessage = showMessage.message;
      },
    });
  }

  closeModal() {
    this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
  }
}
