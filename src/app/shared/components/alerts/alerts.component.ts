import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnChanges {
@Input() showErrorAlert = false;
@Output() onchangeVariable: EventEmitter<boolean> = new EventEmitter<boolean>()

ngOnChanges(changes: SimpleChanges): void {
  if (this.showErrorAlert) {
    setTimeout(() => {
      this.showErrorAlert = false;
      this.closePopup(this.showErrorAlert);
    }, 1000);
  }
}

  closePopup(variableToChange: boolean) {
    this.onchangeVariable.emit(variableToChange);
  }

}
