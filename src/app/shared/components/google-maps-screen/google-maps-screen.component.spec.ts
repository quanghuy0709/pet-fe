import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapsScreenComponent } from './google-maps-screen.component';

describe('GoogleMapsScreenComponent', () => {
  let component: GoogleMapsScreenComponent;
  let fixture: ComponentFixture<GoogleMapsScreenComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GoogleMapsScreenComponent]
    });
    fixture = TestBed.createComponent(GoogleMapsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
