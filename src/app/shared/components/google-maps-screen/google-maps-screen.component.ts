import { Component, EventEmitter, Input, OnChanges, OnInit, Output, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NearMarker, ORIGIN, iLocation } from 'src/app/models/nearkMarker.model';

@Component({
  selector: 'app-google-maps-screen',
  templateUrl: './google-maps-screen.component.html',
  styleUrls: ['./google-maps-screen.component.scss'],
})
export class GoogleMapsScreenComponent implements OnInit, OnChanges {
  @ViewChildren(MapInfoWindow) infoWindowsView?: QueryList<MapInfoWindow>;
  @Input() nearMarkersFromMe: NearMarker[] = [];
  @Output() getCurrentPosition = new EventEmitter<iLocation>();
  center?: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    mapTypeControl: false,
    zoomControl: true,
    scrollwheel: false,
    disableDoubleClickZoom: false,
    maxZoom: 15,
    zoom: 12,
    minZoom: 2,
  };
  currentMarkerPosition: any;

  constructor(private readonly snackBarMat: MatSnackBar) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.currentMarkerPosition) {
      this.nearMarkersFromMe.push(this.currentMarkerPosition);
    }
  }

  ngOnInit(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          const currentPosition: iLocation = {
            formattedAddress: '',
            lat: this.center.lat,
            lng: this.center.lng,
          };
          this.getCurrentPosition.emit(currentPosition);

          this.currentMarkerPosition = new NearMarker({
            name: '',
            surname: '',
            location: {
              formattedAddress: '',
              lat: this.center.lat,
              lng: this.center.lng,
            },
            origin: ORIGIN.NO_ORIGIN,
          });
        },
        (error) => {
          this.snackBarMat.open(error.message, 'Chiudi', { duration: 1500 });
          console.error(error);
        }
      );
    }
  }

  openInfoWindow(marker: MapMarker, windowIndex: number) {
    let curIdx = 0;
    this.infoWindowsView?.forEach((window: MapInfoWindow) => {
      if (windowIndex === curIdx) {
        window.open(marker);
        curIdx++;
      } else {
        window.close();
        curIdx++;
      }
    });
  }
}
