import { AsyncPipe, CommonModule, NgFor } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GoogleMapsModule } from '@angular/google-maps';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { AlertsComponent } from './components/alerts/alerts.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { GoogleMapsScreenComponent } from './components/google-maps-screen/google-maps-screen.component';
import { ModalComponent } from './components/modal/modal.component';
import { MultiSelectAutocompleteComponent } from './components/multi-select-autocomplete/multi-select-autocomplete.component';
import { SelectTypePageComponent } from './components/select-type-page/select-type-page.component';
import { DebounceKeyupDirective } from './directives/debounce-keyup.directive';
import { NavbarComponent } from './navbar/navbar.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { SearchMapComponent } from './search-map/search-map.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ComingSoonComponent } from './components/coming-soon/coming-soon.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PlaceholderPipe } from './pipes/placeholder.pipe';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

const COMPONENTS = [
  NavbarComponent,
  SidebarComponent,
  SearchMapComponent,
  AlertsComponent,
  SelectTypePageComponent,
  ModalComponent,
  GoogleMapsScreenComponent,
  MultiSelectAutocompleteComponent,
  AutocompleteComponent,
  ComingSoonComponent,
];
const DIRECTIVES = [DebounceKeyupDirective];
const MATERIAL_MODULES = [
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatChipsModule,
  MatIconModule,
  MatChipsModule,
  MatCheckboxModule,
  MatInputModule,
  MatSnackBarModule,
  ReactiveFormsModule,
  MatSidenavModule,
  MatToolbarModule,
  FormsModule,
  NgFor,
  AsyncPipe,
];
const MODULES = [GoogleMapsModule];
const PIPES = [CapitalizePipe, PlaceholderPipe];

@NgModule({
  declarations: [NavbarComponent, ...COMPONENTS, ...PIPES, ...DIRECTIVES],
  imports: [CommonModule, RouterModule, ...MATERIAL_MODULES, ...MODULES],
  exports: [...COMPONENTS, ...PIPES, ...DIRECTIVES, ...MATERIAL_MODULES, ...MODULES],
})
export class SharedModule {}
