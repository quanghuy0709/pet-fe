import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { map } from 'rxjs';
import { CODE, CodeGeneral, iCodeGeneralResponse } from 'src/app/models/codeGeneral.model';

@Injectable({ providedIn: 'root' })
export class CodesService {
  private readonly API_URL = 'api/codes';
  constructor(private readonly apiService: ApiService) {}

  getListCode(code: string) {
    return this.apiService
      .get<iCodeGeneralResponse>(`${this.API_URL}?code=${code}`)
      .pipe(map((response) => response.data.map((singleCodeRes) => new CodeGeneral(singleCodeRes))));
  }

  getByCode() {}

  searchBycode(itemCode: string, searchString: string, code: CODE = CODE.PetBreed) {
    return this.apiService
      .get<iCodeGeneralResponse>(`${this.API_URL}?code=${code}${itemCode}&search=${searchString}`)
      .pipe(map((response) => response.data.map((singleCodeRes) => new CodeGeneral(singleCodeRes))));
  }

  searchBycodeCount(itemCode: string, code: CODE = CODE.PetBreed) {
    return this.apiService
      .get<iCodeGeneralResponse>(`${this.API_URL}?code=${CODE.PetBreed}${itemCode}`)
      .pipe(map((response) => response.data.map((singleCodeRes) => new CodeGeneral(singleCodeRes))));
  }
}
