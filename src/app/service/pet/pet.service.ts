import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Pet } from 'src/app/models/pet.model';
import { map } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PetService {
  private readonly API_URL = 'api/pets';
  constructor(private readonly apiService: ApiService) {}

  createPet(pet: Pet) {
    const customPet = {
      name: pet.name,
      sex: pet.sex,
      dateOfBirth: pet.dateOfBirth,
      kind: pet.kind,
      breed: pet.breed,
      imgProfile: pet.imgProfile,
    };
    return this.apiService.post(this.API_URL, customPet);
  }

  updatePet(idPet: string, pet: Pet) {
    const customPet = {
      name: pet.name,
      sex: pet.sex,
      dateOfBirth: pet.dateOfBirth,
      kind: pet.kind,
      breed: pet.breed,
      imgProfile: pet.imgProfile,
      distinguishingMarks: pet?.distinguishingMarks,
      coated: pet?.coated,
      microchip: pet?.microchip,
    };
    return this.apiService.put(`${this.API_URL + '/' + idPet}`, customPet);
  }

  delete(idPet: string) {
    return this.apiService.delete(`${this.API_URL + '/' + idPet}`);
  }

  getPets() {
    return this.apiService.get(this.API_URL).pipe(map((responsePets: any) => responsePets.data.map((resPet: any) => new Pet(resPet))));
  }

  getPet(id: any) {
    return this.apiService.get(`${this.API_URL}/${id}`).pipe(map((responsePet: any) => new Pet(responsePet.data)));
  }
}
