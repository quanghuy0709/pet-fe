import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { HealthCard, iHealthCardResponse } from 'src/app/models/healthCard.model';
import { MedicalHistory, iMedicalHistoryResponse } from 'src/app/models/medicalHistory.model';
import { TreatmentPlan, iTreatmentPlanResponse } from 'src/app/models/treatmentPlan.model';
import { ApiService } from '../api.service';

@Injectable({ providedIn: 'root' })
export class HealthcareService {
  private readonly API_URL = 'api/health-care';
  constructor(private readonly apiService: ApiService) {}

  getHealthCards(optionsQuery?: any) {
    const queryParamsString = defineQueryParams(optionsQuery);
    return this.apiService
      .get<iHealthCardResponse>(`${this.API_URL}/health-cards${queryParamsString}`)
      .pipe(map((value) => value.data.map((data) => new HealthCard(data))));
  }

  getMedicalHistories(optionsQuery?: any) {
    const queryParamsString = defineQueryParams(optionsQuery);
    return this.apiService
      .get<iMedicalHistoryResponse>(`${this.API_URL}/medical-histories${queryParamsString}`)
      .pipe(map((value) => value.data.map((data) => new MedicalHistory(data))));
  }

  getTreatmentPlans(optionsQuery?: any) {
    const queryParamsString = defineQueryParams(optionsQuery);
    return this.apiService
      .get<iTreatmentPlanResponse>(`${this.API_URL}/treatment-plans${queryParamsString}`)
      .pipe(map((value) => value.data.map((data) => new TreatmentPlan(data))));
  }

  manageHealthCard(type: 'edit' | 'add' | 'delete', id?: any, body?: any) {
    switch (type) {
      case 'add':
        return this.create(body);
      case 'edit':
        return this.edit(body, id);
      case 'delete':
        return this.delete(id);
    }
  }

  create(body: any) {
    return this.apiService.post(`${this.API_URL}/health-cards`, body);
  }

  edit(body: any, id: any) {
    return this.apiService.put(`${this.API_URL}/health-cards/${id}`, body);
  }

  delete(id: string) {
    return this.apiService.delete(`${this.API_URL}/health-cards/${id}`);
  }

  createMedicalHistory(body: any) {
    return this.apiService.post(`${this.API_URL}/medical-histories`, body);
  }

  createTreatmentPlan(body: any) {
    return this.apiService.post(`${this.API_URL}/treatment-plans`, body);
  }
}
function defineQueryParams(optionsQuery: any) {
  const queryKeys = Object.keys(optionsQuery);
  const queryParamsString = queryKeys
    .map((queryKey) => {
      return `${queryKey}=${optionsQuery[queryKey]}`;
    })
    .join('&');
  return queryParamsString ? '?' + queryParamsString : '';
}
