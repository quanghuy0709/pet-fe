import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
// import { User } from 'src/app/models/user.model';
// import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly API_URL = 'api/users';

  constructor(private readonly apiService: ApiService) {}

  getUsers() {
    return this.apiService.get(this.API_URL);
  }
}
