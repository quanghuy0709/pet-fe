import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ModalService {
  modeRegister: 'tedbuddy' | 'professional' = 'tedbuddy';
  private $showLogin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private $showRegisterComponent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private $showFormRegister: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private $showMessage: BehaviorSubject<{ type: 'warning' | 'error' | 'confirm'; message?: string; visible: boolean }> = new BehaviorSubject<{
    type: 'warning' | 'error' | 'confirm';
    title?: string;
    message?: string;
    visible: boolean;
  }>({
    visible: false,
    type: 'confirm',
  });

  constructor() {}

  get showLogin(): Observable<boolean> {
    return this.$showLogin.asObservable();
  }

  set showLogin(value: boolean) {
    this.$showLogin.next(value);
  }

  get showRegisterComponent(): Observable<boolean> {
    return this.$showRegisterComponent.asObservable();
  }

  set showRegisterComponent(value: boolean) {
    this.$showRegisterComponent.next(value);
  }

  get showFormRegister(): Observable<boolean> {
    return this.$showFormRegister.asObservable();
  }

  set showFormRegister(value: boolean) {
    this.$showFormRegister.next(value);
  }

  get showMessage(): Observable<{ type: 'warning' | 'error' | 'confirm'; title?: string; message?: string; visible: boolean }> {
    return this.$showMessage.asObservable();
  }

  set showMessage(value: { type: 'warning' | 'error' | 'confirm'; title?: string; message?: string; visible: boolean }) {
    this.$showMessage.next(value);
  }
}
