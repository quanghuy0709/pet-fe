import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PdfService {
  constructor(private readonly apiService: ApiService) {}

  downloadPdf(idPet: string) {
    return this.apiService
      .get<Blob>(`api/health-care/reports/health-card/${idPet}`, { 'Content-Type': 'application/json', responseType: 'blob' }, 'blob' as 'json')
      .pipe(
        map((value) => {
          const file = new Blob([value], { type: 'application/pdf' });
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL);
        })
      );
  }
}
