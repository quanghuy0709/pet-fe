import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { LocalstorageService } from '../localstorage.service';
import { Token } from 'src/app/models/token.model';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private readonly router: Router, private readonly localstorageService: LocalstorageService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<any> {
    if (request.url.includes(`login`) || request.url.includes(`register`) || request.url.includes(`assets`)) return next.handle(request);
    // const token = JSON.parse(localStorage.getItem('token')!);
    const token = new Token(this.localstorageService.get('token'));
    if (token && token.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token.token}`,
        },
      });
      return next.handle(request).pipe(
        catchError((err: any) => {
          if (err.status !== 401) {
            return throwError(() => err);
          }
          return this.router.navigate(['auth', 'login']);
        })
      );
    } else return of(this.router.navigate(['auth', 'login']));

    // return next.handle(request);
  }
}
