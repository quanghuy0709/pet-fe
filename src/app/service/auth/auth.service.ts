import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Token } from 'src/app/models/token.model';
import { ApiService } from '../api.service';
import { LocalstorageService } from '../localstorage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly API_URL = 'auth';
  constructor(private readonly apiService: ApiService, private readonly localstorageService: LocalstorageService) {}

  login(body: any) {
    return this.apiService.post(`${this.API_URL}/login`, body).pipe(
      map((tokenResponse: any) => {
        const token = new Token(tokenResponse);
        this.localstorageService.set('token', token);
        return token;
      })
    );
  }

  logout() {
    return this.apiService.post(`${this.API_URL}/logout`, {});
  }

  register(body: any, mode: 'tedbuddy' | 'professional') {
    return this.apiService.post(mode === 'professional' ? `${this.API_URL}/professional/register` : `${this.API_URL}/register`, body);
  }

  profile() {
    // const headers = { Authorization: `Bearer ${token.token}` };
    return this.apiService.get(`${this.API_URL}/profile`);
    // .pipe(map((value: any) => value));
  }

  updateProfile(body: any) {
    return this.apiService.patch(`${this.API_URL}/profile`, body);
  }

  //   refreshToken(token: Token) {
  //     return this.apiService
  //       .post('api/magento/login', {
  //         grantType: 'refresh_token',
  //         refreshToken: token.refreshToken,
  //       })
  //       .pipe(map((v: any) => new Token(v)));
  //   }
}
