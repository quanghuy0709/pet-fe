import { Injectable } from '@angular/core';
import { NearMarker, iLocation, iNearMarkerResponse } from 'src/app/models/nearkMarker.model';
import { ApiService } from '../api.service';
import { map } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GoogleMapService {
  center?: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'hybrid',
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    maxZoom: 15,
    minZoom: 2,
  };
  currentMarkerPosition: any = {};
  private readonly API_URL = 'api/business';
  constructor(private readonly apiService: ApiService) {}

  search(location: iLocation) {
    return this.apiService
      .get<iNearMarkerResponse>(`${this.API_URL}/search/near?lng=${location.lng}&lat=${location.lat}`)
      .pipe(map((response: iNearMarkerResponse) => response.data.map((element) => new NearMarker(element))));
  }

  getcurrentPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.center = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          return this.center;
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
