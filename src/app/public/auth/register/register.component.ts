import { Component, ElementRef, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/service/auth/auth.service';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  @ViewChild('buttonRegister') buttonRegister?: ElementRef;
  show: boolean = false;
  selectedRegister: boolean = false;
  user?: User;
  myForm?: FormGroup;
  typeRegister: 'tedbuddy' | 'professional' = 'tedbuddy';
  showErrorAlert = false;
  isLoading = false;

  typeHealthcards: any[] = [
    {
      title: 'Ted Buddy',
      icon: 'assets/sprinted/register/owner_dog_icon.svg',
      type: 'tedbuddy',
    },
    {
      title: 'Veterinario',
      icon: 'assets/sprinted/register/vet_icon.svg',
      type: 'professional',
    },
  ];
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly modalService: ModalService,
    private readonly formBuilder: FormBuilder
  ) {
    this.modalService.showRegisterComponent.subscribe({
      next: (value) => (this.show = value),
    });
    this.modalService.showFormRegister.subscribe({
      next: (value) => (this.selectedRegister = value),
    });
  }
  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      surname: [''],
      email: [''],
      username: [''],
      password: [''],
      imgProfile: [''],
      address: [''],
      flagAcceptanceTerms: [false],
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  closeModal() {
    this.show = false;
    this.modalService.showRegisterComponent = false;
    this.selectedRegister = false;
    this.typeRegister = 'tedbuddy';
  }

  showLogin() {
    this.closeModal();
    this.modalService.showLogin = true;
  }

  register() {
    console.log('passo di qui');
    this.isLoading = true;
    this.myForm?.controls['username'].patchValue(this.myForm?.controls['email'].value);
    const valueForm = this.myForm?.value;
    this.authService.register(valueForm, this.modalService.modeRegister).subscribe({
      next: (value) => {
        console.log('OK');
        this.isLoading = false;
        this.myForm?.reset();
        this.closeModal();
        this.router.navigate(['']);
      },
      error: () => {
        console.log('KO');
        this.showErrorAlert = true;
        this.isLoading = false;
        // this.myForm?.reset();
        // this.closeModal();
      },
    });
  }

  selectForm(type: any) {
    this.modalService.modeRegister = type;
    this.typeRegister = type;
    this.selectedRegister = true;
  }
}
