import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-select-type-register',
  templateUrl: './select-type-register.component.html',
  styleUrls: ['./select-type-register.component.scss'],
})
export class SelectTypeRegisterComponent implements AfterViewInit {
  @Input() showModal: boolean = false;
  @Input() items: any[] = [];
  @Output() selectedTypeValue: EventEmitter<string> = new EventEmitter<string>();
  showTypeRegister: boolean = false;

  constructor(private readonly modalService: ModalService) {
    this.modalService.showRegisterComponent.subscribe({
      next: (value) => (this.showTypeRegister = value),
    });
  }

  ngAfterViewInit(): void {}

  closeModal() {
    this.showTypeRegister = false;
    this.modalService.showRegisterComponent = false;
  }
}
