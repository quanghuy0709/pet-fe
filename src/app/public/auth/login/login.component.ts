import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { map, switchMap } from 'rxjs';
import { AuthService } from 'src/app/service/auth/auth.service';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { ModalService } from 'src/app/service/modal/modal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements AfterViewInit {
  @ViewChild('buttonLogin') buttonLogin?: ElementRef;
  showLogin: boolean = false;
  isLoading = false;
  isPasswordVisibile = false;
  showErrorAlert = false;
  typePassword = 'password';
  user = {
    username: '',
    password: '',
  };

  constructor(
    private readonly authService: AuthService,
    private readonly localstorageService: LocalstorageService,
    private readonly router: Router,
    private readonly modalService: ModalService
  ) {
    this.modalService.showLogin.subscribe({
      next: (value) => {
        this.showLogin = value;
      },
    });
  }

  ngAfterViewInit(): void {}

  closeModal() {
    this.modalService.showLogin = false;
    this.setDefaultVariables();
  }

  showRegister() {
    this.closeModal();
    this.modalService.showRegisterComponent = true;
  }

  showPassword() {
    this.isPasswordVisibile = !this.isPasswordVisibile;
    this.typePassword = this.isPasswordVisibile ? 'text' : 'password';
  }

  login() {
    this.isLoading = true;
    this.authService
      .login(this.user)
      .pipe(
        switchMap(() => this.authService.profile()),
        map((user) => {
          this.localstorageService.set('user', user);
          return user;
        })
      )
      .subscribe({
        next: (user) => {
          this.isLoading = false;
          this.modalService.showLogin = false;
          this.router.navigate(['private']);
          // location.reload();
        },
        error: () => {
          this.isLoading = false;
          this.showErrorAlert = true;
        },
      });
  }

  private setDefaultVariables() {
    this.showLogin = false;
    this.isPasswordVisibile = false;
    this.isLoading = false;
    this.typePassword = 'password';
  }

  showFormRegister() {
    this.modalService.showLogin = false;
    this.modalService.showRegisterComponent = true;
    this.modalService.showFormRegister = true;
  }
}
