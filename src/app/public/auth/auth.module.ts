import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectTypeRegisterComponent } from './register/select-type-page/select-type-register.component';

@NgModule({
  declarations: [AuthComponent, LoginComponent, RegisterComponent, SelectTypeRegisterComponent],
  imports: [CommonModule, AuthRoutingModule, FormsModule, ReactiveFormsModule, MatDatepickerModule, MatNativeDateModule, SharedModule],
  exports: [LoginComponent, RegisterComponent],
})
export class AuthModule {}
