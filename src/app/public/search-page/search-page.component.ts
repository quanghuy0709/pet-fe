import { Component } from '@angular/core';
import { NearMarker, iLocation } from 'src/app/models/nearkMarker.model';
import { GoogleMapService } from 'src/app/service/googleMap/googleMap.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent {
  searchText: string = '';
  locationSelected: any;
  markers: any[] = [];
  persons: NearMarker[] = [];

  constructor(private readonly googleMapService: GoogleMapService) {}

  search() {
    this.googleMapService.search(this.locationSelected.location).subscribe({
      next: (response) => {
        this.persons = response;
        response.map((item) => this.markers.push(item.marker));
      },
    });
  }
}
