import { AfterViewInit, Component, ElementRef, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalstorageService } from 'src/app/service/localstorage.service';
import { ModalService } from 'src/app/service/modal/modal.service';
import { ScriptJsLoaderService } from 'src/app/service/script-js-loader.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('main') main?: ElementRef;
  @ViewChild('header') header?: ElementRef;
  showLogin: boolean = false;
  typeUser: 'tedBuddy' | 'professional' = 'tedBuddy';
  searchText: string = '';
  externalHtml: any;
  @ViewChild(ModalComponent) modal: any;
  type?: string;
  modalImage?: string;
  modalTitle?: string;
  modalMessage?: string;
  isLoggedUser: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly modalService: ModalService,
    private readonly scriptJsLoaderService: ScriptJsLoaderService,
    private readonly localstorageService: LocalstorageService
  ) {}
  ngOnDestroy(): void {
    document.removeEventListener('scroll', () => {});
  }
  ngAfterViewInit(): void {
    document.addEventListener('scroll', () => {});
    // this.loadScripts();
    console.log(this.main);

    if (this.main) this.main.nativeElement.style.marginTop = this.header?.nativeElement?.offsetHeight + 'px';
  }

  ngOnInit(): void {
    this.modalService.showMessage.subscribe((data) => {
      const { type, title, message, visible } = data;
      this.modalImage = '/assets/modal/modal-' + type + '.jpg';
      this.modalTitle = title || type;
      this.modalMessage = message;
    });
    this.isLoggedUser = history.state['isLogged'];
  }

  public get user(): any {
    return this.localstorageService.get('user');
  }

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.scriptJsLoaderService
      .load('mainjs')
      .then((data) => {
        console.log('script loaded ', data);
      })
      .catch((error) => console.error(error));
  }

  showModal(type: 'login' | 'registrati', mode: 'tedbuddy' | 'professional' = 'tedbuddy') {
    if (type == 'login') {
      this.modalService.showLogin = true;
    } else {
      this.modalService.showRegisterComponent = true;
      this.modalService.modeRegister = mode;
    }
  }

  showModalMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Medico Veterinario o Professionista',
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }

  showModalSprintedProMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Sprinted Pro',
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }
  showModalAiutoMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Aiuto',
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }
  showModalScaricaMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: "Scarica l'App",
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }
  showModalVisitaStudioMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Visita in studio',
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }
  showModalVisitaOnlineMessage() {
    this.modalService.showMessage = {
      type: 'warning',
      title: 'Visita online',
      message: 'Funzionalità non ancora disponibile',
      visible: true,
    };
  }

  closeModal() {
    this.modalService.showMessage = { type: 'warning', message: 'Funzionalità non ancora disponibile', visible: false };
  }

  showFormRegister() {
    this.modalService.showRegisterComponent = true;
    this.modalService.showFormRegister = true;
  }

  selectModeUser(typeUser: 'tedBuddy' | 'professional') {
    this.typeUser = typeUser;
  }

  search() {
    this.router.navigate(['public', 'search-page']);
  }

  loadJsCssFile(url: any, type: string) {
    let node: any;
    if (type === 'script') {
      node = document.createElement('script');
      node.src = url;
      node.type = 'text/javascript';
    } else {
      node = document.createElement('link');
      node.href = url;
      node.rel = 'stylesheet';
    }
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  goToDashboard() {
    this.router.navigate(['private']);
  }
}
