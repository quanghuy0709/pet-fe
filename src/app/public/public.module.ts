import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { ModalService } from '../service/modal/modal.service';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [],
  imports: [CommonModule, PublicRoutingModule, MatCardModule],
})
export class PublicModule {}
