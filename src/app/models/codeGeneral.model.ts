export interface iCodeGeneralResponse {
  success: boolean;
  data: iCodeGeneral[];
}

export interface iCodeGeneral {
  _id: string;
  code: CODE;
  itemCode?: string;
  description: string;
  description1: string;
  description2: string;
  description3: string;
  __v?: number;
}

export enum CODE {
  BusinessService = 'BUSINESS_SERVICE',
  BusinessSpecialization = 'BUSINESS_SPECIALIZATION',
  Empty = '',
  PetBreedCane = 'PET_BREED_CANE',
  PetBreedCavallo = 'PET_BREED_CAVALLO',
  PetBreedConiglio = 'PET_BREED_CONIGLIO',
  PetBreedGatto = 'PET_BREED_GATTO',
  PetBreedRettile = 'PET_BREED_RETTILE',
  PetBreedTopo = 'PET_BREED_TOPO',
  PetBreedUccello = 'PET_BREED_UCCELLO',
  PetBreed = 'PET_BREED_',
  PetKind = 'PET_KIND',
  PetSex = 'PET_SEX',
  SexPet = 'SEX_PET',
  TypologyPrescription = 'TYPOLOGY_PRESCRIPTION',
}

export class CodeGeneral {
  id: string = '';
  code?: CODE;
  itemCode: string = '';
  description: string = '';

  constructor(obj: iCodeGeneral) {
    this.id = obj._id;
    this.code = obj.code;
    this.itemCode = obj.itemCode ?? CODE.Empty;
    this.description = obj.description;
  }
}
