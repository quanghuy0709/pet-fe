interface iToken {
  token: string;
}

export class Token {
  token: string = '';

  constructor(token?: iToken) {
    this.token = token?.token ?? '';
  }
}
