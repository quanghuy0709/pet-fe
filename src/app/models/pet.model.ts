import { DateTime } from 'luxon';

export interface iPet {
  _id: string;
  name: string;
  sex: 'Maschio' | 'Femmina';
  dateOfBirth: string;
  kind: string;
  breed: string;
  extraInformations: {};
  imgProfile: string;
  distinguishingMarks: string;
  microchip: string;
  coated: string;
}

export class Pet {
  id: string = '';
  name: string = '';
  sex: string = '';
  dateOfBirth: string = '';
  kind: string = '';
  breed: string = '';
  extraInformations: any = {};
  imgProfile: string = '';
  isSelected = false;
  distinguishingMarks: string = '';
  microchip: string = '';
  coated: string = '';
  constructor(pet: iPet) {
    this.id = pet._id;
    this.name = pet?.name;
    // this.sex = this.setSexPet(pet.sex);
    this.sex = pet.sex;
    this.dateOfBirth = pet?.dateOfBirth != '' ? DateTime.fromISO(new Date(pet?.dateOfBirth).toISOString()).toFormat('yyyy-MM-dd') : '';
    this.kind = pet?.kind;
    this.breed = pet?.breed;
    this.extraInformations = pet?.extraInformations ?? {};
    this.imgProfile = pet?.imgProfile;
    this.distinguishingMarks = pet?.distinguishingMarks;
    this.coated = pet?.coated;
    this.microchip = pet?.microchip;
  }

  setSexPet(sex: 'M' | 'F') {
    return sex === 'M' ? 'Maschio' : 'Femmina';
  }
}
