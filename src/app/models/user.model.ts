import { DateTime } from 'luxon';

export interface iUser {
  name: string;
  surname: string;
  email: string;
  dateOfBirth: string;
  phone: {
    prefix: number;
    number: number;
  };
  flagPrivacy: boolean;
  kind: string;
  imgProfile: string;
  address: {
    formattedAddress: {
      type: string;
    };
    location: {
      lat: {
        type: number;
      };
      lng: {
        type: number;
      };
    };
  };
}

export class User {
  name: string = '';
  surname: string = '';
  email: string = '';
  dateOfBirth: string = '';
  flagPrivacy: boolean = false;
  phone: any = {};
  kind: string = '';
  imgProfile: string = '';
  address = {
    formattedAddress: {
      type: '',
    },
    location: {
      lat: {
        type: 0,
      },
      lng: {
        type: 0,
      },
    },
  };

  constructor(user: iUser) {
    this.name = user?.name;
    this.surname = user?.surname;
    this.email = user.email;
    this.dateOfBirth =
      user?.dateOfBirth != ''
        ? DateTime.fromISO(new Date(user?.dateOfBirth).toISOString()).toFormat(
            'yyyy-MM-dd'
          )
        : '';
    this.kind = user?.kind;
    this.flagPrivacy = user.flagPrivacy;
    this.phone = user?.phone;
    this.imgProfile = user?.imgProfile;
    this.address = user?.address;
  }
}
