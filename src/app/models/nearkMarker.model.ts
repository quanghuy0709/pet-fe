import { MapMarker } from '@angular/google-maps';

export interface iNearMarkerResponse {
  data: iNearMarker[];
}

export interface iNearMarker {
  name: string;
  surname: string;
  location: iLocation;
  origin: ORIGIN;
}

export interface iLocation {
  formattedAddress: string;
  lng: number;
  lat: number;
}

export enum ORIGIN {
  SPRINTED = 'SPRINTED',
  GOOGLE = 'GOOGLE',
  NO_ORIGIN = '',
}

export class NearMarker {
  name: string = '';
  surname: string = '';
  location: iLocation;
  origin: ORIGIN;
  marker: any;
  titleUrl: string | boolean;

  constructor(obj: iNearMarker) {
    this.name = obj.name;
    this.surname = obj.surname;
    this.location = obj.location;
    this.origin = obj.origin;
    this.titleUrl = obj.origin == ORIGIN.GOOGLE && `https://www.google.com/search?q=${obj.name}`;
    this.marker = {
      position: {
        lat: obj.location.lat,
        lng: obj.location.lng,
      },
      title: obj.name,
      label: {
        color: 'white',
        text: obj.location.formattedAddress,
      },
      options: {
        animation: google.maps.Animation.DROP,
        icon: this.getIcon(obj.origin),
        scaledSize: new google.maps.Size(50, 50), // scaled size
      },
    };
  }

  private getIcon(origin: ORIGIN) {
    switch (origin) {
      case ORIGIN.GOOGLE:
        return `assets/map/default_marker.svg`;
      case ORIGIN.SPRINTED:
        return 'assets/map/sprinted_marker.svg';
      case ORIGIN.NO_ORIGIN:
        return 'assets/map/me_marker.svg';
    }
  }
}
