// export interface iTreatmentPlanResponse {
//   success: boolean;
//   data: iTreatmentPlan[];
// }

// export interface iTreatmentPlan {
//   _id: string;
//   companyOfDrug: string;
//   dosage: string;
//   times: string[];
//   recommendation: string;
//   usedByTreatment: boolean;
//   startDate: null;
//   endDate: null;
//   pet: string;
//   createdAt: Date;
//   updatedAt: Date;
//   __v: number;
//   drugName?: string;
// }
export interface iTreatmentPlanResponse {
  success: boolean;
  data: iTreatmentPlan[];
}

export interface iTreatmentPlan {
  _id: string;
  drugName: string;
  quantity: string;
  startDate: Date;
  endDate: Date;
  frequency: string;
  name: string;
  ifRepeats: boolean;
  reminderEnabled: boolean;
  times: string[];
  status: string;
  pet: string;
  schedules: Schedule[];
}

export interface Schedule {
  dateStart: Date;
  dateEnd: Date;
  note: string;
  status: string;
  notified: boolean;
  _id: string;
}

export class TreatmentPlan {
  id: string = '';
  name: string = '';
  drugName?: string;
  times: string[];
  pet: string = '';
  startDate: Date = new Date();
  endDate: Date = new Date();
  icon: string = '';
  schedules: Schedule[];
  isOpenAccordion: boolean = false;

  constructor(obj: iTreatmentPlan) {
    this.id = obj._id;
    this.name = obj.name;
    this.drugName = obj.drugName;
    this.times = obj.times;
    this.pet = obj.pet;
    this.startDate = obj.startDate ?? new Date();
    this.endDate = obj.endDate ?? new Date();
    this.schedules = obj.schedules;
    this.icon = 'assets/sprinted/healthcare/treatment_plan_icon.svg';
  }
}
