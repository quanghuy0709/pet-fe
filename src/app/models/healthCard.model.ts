export interface iHealthCardResponse {
  success: boolean;
  data: iHealthCard[];
}

export interface iHealthCard {
  recall: iRecall;
  _id: string;
  type: TYPE_HEARTHCARD;
  name: string;
  lot: string;
  vet: string;
  whenApplied: null;
  note: string;
  pet: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface iRecall {
  dateOfRecall: null;
}

export enum TYPE_HEARTHCARD {
  VACCINE = 'vaccine',
  ANTIPARASITIC = 'antiparasitic',
}

export class HealthCard {
  id: string = '';
  type: TYPE_HEARTHCARD = TYPE_HEARTHCARD.VACCINE;
  name: string = '';
  veterinarian: string = '';
  whenApplied: any;
  note: string = '';
  lot: string = '';
  pet: string = '';
  recall?: iRecall;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();
  icon: string = '';
  isOpenAccordion: boolean = false;

  constructor(obj: iHealthCard) {
    this.id = obj._id;
    this.type = obj.type;
    this.name = obj.name;
    this.veterinarian = obj.vet;
    this.whenApplied = obj.whenApplied;
    this.lot = obj.lot;
    this.note = obj.note;
    this.pet = obj.pet;
    this.recall = obj.recall;
    this.createdAt = obj.createdAt;
    this.updatedAt = obj.updatedAt;
    this.icon = `assets/sprinted/healthcare/${obj.type}_icon.svg`;
  }
}
