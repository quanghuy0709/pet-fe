export interface iMedicalHistoryResponse {
  success: boolean;
  data: iMedicalHistory[];
}

export interface iMedicalHistory {
  detail: iDetail;
  _id: string;
  type: string;
  pet: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
}

export interface iDetail {
  prescription?: iPrescription;
  medicalExamination?: iMedicalExamination;
}

export interface iMedicalExamination {
  typeOfVisit: string;
  vet: string;
  vetClinic: string;
  whenApplied: null;
  note: string;
}

export interface iPrescription {
  tipologyOfPrescription: string;
  vet: string;
  nrPrescription: string;
  pin: string;
  whenApplied: null;
}

export class MedicalHistory {
  id: string = '';
  type: string = '';
  detail?: iDetail;
  createdAt: Date = new Date();
  icon: string = '';
  isOpenAccordion: boolean = false;

  constructor(obj: iMedicalHistory) {
    this.id = obj._id;
    this.type = obj.type;
    this.detail = obj.detail;
    this.createdAt = obj.createdAt;
    this.icon = `assets/sprinted/healthcare/${obj.type}_icon.svg`;
  }
}
