import { CanActivateFn, Router } from '@angular/router';
import { LocalstorageService } from '../service/localstorage.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  const router: any = inject(Router).getCurrentNavigation()?.extras.state;
  const isLogged: boolean = router?.isLogged ?? false;
  const user = inject(LocalstorageService).get('user');
  if (!user || isLogged) {
    return true;
  } else {
    return inject(Router).navigate(['private']);
  }
};
