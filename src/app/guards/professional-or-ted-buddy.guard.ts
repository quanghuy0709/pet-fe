import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { LocalstorageService } from '../service/localstorage.service';

export const tedBuddyGuard: CanActivateFn = (route, state) => {
  const user = inject(LocalstorageService).get('user');
  if (user.data.kind == 'ted-buddy') {
    return true;
  } else if (user.data.kind == 'professional') {
    return inject(Router).navigate(['private', 'professional']);
  }
  return false;
};
export const professionalGuard: CanActivateFn = (route, state) => {
  const user = inject(LocalstorageService).get('user');
  if (user.data.kind == 'ted-buddy') {
    return inject(Router).navigate(['private', 'ted-buddy']);
  } else if (user.data.kind == 'professional') {
    return true;
  }
  return false;
};
