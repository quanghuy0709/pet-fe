import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { professionalOrTedBuddyGuard } from './professional-or-ted-buddy.guard';

describe('professionalOrTedBuddyGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => professionalOrTedBuddyGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
