import { CanActivateFn, Router } from '@angular/router';
import { LocalstorageService } from '../service/localstorage.service';
import { inject } from '@angular/core';

export const authPrivateGuard: CanActivateFn = (route, state) => {
  // return true;
  const user = inject(LocalstorageService).get('user');
  if (user) {
    return true;
  }
  return inject(Router).navigate(['public']);
};
